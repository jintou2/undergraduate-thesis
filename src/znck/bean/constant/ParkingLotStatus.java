package znck.bean.constant;

public class ParkingLotStatus {

	/**
	 * 车场未运行
	 */
	public static final int not_running = 0;
	
	/**
	 * 车场运行中
	 */
	public static final int running = 1;
}
