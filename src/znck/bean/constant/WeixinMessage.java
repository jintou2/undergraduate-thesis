package znck.bean.constant;

public class WeixinMessage {

	/**
	 * 关注微信号消息回复
	 */
	public static final String welcome = "亲(づ￣3￣)づ╭❤～，欢迎~!";
	
	/**
	 * 未开发功能消息回复
	 */
	public static final String noFunction = "亲!程序猿正快马加鞭的开发中，很希望为您服务呢O(∩_∩)O~!";
	
}
