package znck.bean.constant;

public class VerificationCode {

	/**
	 * 验证码发送事件:账号注册
	 */
	public static final String register = "账号注册";
	
	/**
	 * 验证码发送事件:修改密码
	 */
	public static final String password_modify = "修改密码";
}
