package znck.bean.constant;

public class Price {

	/**
	 * 30分钟以内的价格,单位（元）
	 */
	public static final double priceIn30 = 5.0;
	
	/**
	 * 30分钟以后的价格,单位(元)
	 */
	public static final double priceOut30 = 4.0;
}
