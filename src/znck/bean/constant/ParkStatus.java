package znck.bean.constant;

public class ParkStatus {

	/**
	 * 订单失败
	 */
	public static final int failure = 0;
	
	/**
	 * 已预订,未入库
	 */
	public static final int odered = 1;
	
	/**
	 * 正在进行，已入库
	 */
	public static final int oder_underway = 2;
	
	/**
	 * 订单取消
	 */
	public static final int oder_cancel = 3;
	
	/**
	 * 已完成
	 */
	public static final int oder_finish = 4;
	
	/**
	 * 还有未完成订单（订单重复）
	 */
	public static final int oder_repeat = 5;
}
