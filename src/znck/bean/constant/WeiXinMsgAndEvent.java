package znck.bean.constant;

public class WeiXinMsgAndEvent {

	/**
	 * 请求消息类型:文本
	 */
	public static final String MsgTypeText = "text";
	
	/**
	 * 请求消息类型:图片
	 */
	public static final String MsgTypeImage = "image";
	
	/**
	 * 请求消息类型:语音
	 */
	public static final String MsgTypeVoice = "voice";
	
	/**
	 * 请求消息类型:视频
	 */
	public static final String MsgTypeVideo = "video";
	
	/**
	 * 请求消息类型:小视频
	 */
	public static final String MsgTypeShortvideo = "shortvideo";
	
	/**
	 * 请求消息类型:地理位置
	 */
	public static final String MsgTypeLocation = "location";
	
	/**
	 * 请求消息类型:链接
	 */
	public static final String MsgTypeLink = "link";
	
	
	/**
	 * 请求消息类型:事件
	 */
	public static final String MsgTypeEvent = "event";
	
	
	/**
	 * 事件类型:订阅
	 */
	public static final String EventTypeSubscribe = "subscribe";
	
	/**
	 * 事件类型:取消订阅
	 */
	public static final String EventTypeUnsubscribe = "unsubscribe";
	
	/**
	 * 事件类型:扫描二维码(已关注之后扫描，关注之前扫描就是订阅事件)
	 */
	public static final String EventTypeSCAN = "SCAN";
	
	/**
	 * 事件类型:上报地理位置
	 */
	public static final String EventTypeLOCATION= "LOCATION";
}
