package znck.bean.dto;

import java.util.ArrayList;
import java.util.List;

public class RpklCartype extends Rparkinglot {

	/**
	 * 停车场可停车辆类型
	 */
	private List<Rcartype> cartypes = new ArrayList<>();

	public List<Rcartype> getCartypes() {
		return cartypes;
	}

	public void setCartypes(List<Rcartype> cartypes) {
		this.cartypes = cartypes;
	}
	
	
}
