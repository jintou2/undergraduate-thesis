package znck.bean.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import znck.hb.entity.Complaints;
import znck.hb.entity.Evaluation;
import znck.hb.entity.ParkingLot;
import znck.hb.entity.User;

public class Rpark {

	/**
	 * 标识属性
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	/**
	 * 车辆牌照
	 */
	private String car_number;
	
	/**
	 * 用户
	 */
	private User user;
	
	/**
	 * 预计到达时间
	 */
	private Date EstimateArrival;
	
	/**
	 * 开始时间
	 */
	private Date startdate;
	
	/**
	 * 结束时间
	 */
	private Date enddate;
	
	/**
	 * 停车场
	 */
	private ParkingLot parking_lot;
	
	/**
	 * 停车状态
	 */
	private int status;
	
	/**
	 * 停车费用
	 */
	private double price;
	
	/**
	 * 评价
	 */
	private List<Evaluation> evaluations = new ArrayList<>();
	
	/**
	 * 投诉
	 */
	@OneToMany(targetEntity=Complaints.class, fetch=FetchType.LAZY)
	@JoinColumn(name="complaint_id" , referencedColumnName="id")
	private List<Complaints> complaints = new ArrayList<>();

	public Date getEstimateArrival() {
		return EstimateArrival;
	}

	public void setEstimateArrival(Date estimateArrival) {
		EstimateArrival = estimateArrival;
	}

	public String getCar_number() {
		return car_number;
	}

	public void setCar_number(String car_number) {
		this.car_number = car_number;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public ParkingLot getParking_lot() {
		return parking_lot;
	}

	public void setParking_lot(ParkingLot parking_lot) {
		this.parking_lot = parking_lot;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public List<Evaluation> getEvaluations() {
		return evaluations;
	}

	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}

	public List<Complaints> getComplaints() {
		return complaints;
	}

	public void setComplaints(List<Complaints> complaints) {
		this.complaints = complaints;
	}
	
	
}
