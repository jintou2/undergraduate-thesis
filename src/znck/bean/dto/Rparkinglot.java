package znck.bean.dto;

public class Rparkinglot {

	/**
	 * 用于返回参数，消除延迟加载
	 */
	
	/**
	 * 标识属性
	 */
	private int id;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 经度
	 */
	private double longitude;
	
	/**
	 * 纬度
	 */
	private double latitude;
	
	/**
	 * 区域编号
	 */
	private int areaId;
	
	/**
	 * 车场地址
	 */
	private String address;
	
	/**
	 * 停车位
	 */
	private int park_number;
	
	/**
	 * 剩余停车位
	 */
	private int surplus_park_num;
	
	/**
	 * 车场照片
	 */
	private String image;
	
	/**
	 * 状态
	 */
	private int status;
	
	/**
	 * 起始价
	 */
	private double init_price;
	
	/**
	 * 单价
	 */
	private double per_hour_price;
	
	/**
	 * 停车场简介
	 */
	private String description;

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getPark_number() {
		return park_number;
	}

	public void setPark_number(int park_number) {
		this.park_number = park_number;
	}

	public int getSurplus_park_num() {
		return surplus_park_num;
	}

	public void setSurplus_park_num(int surplus_park_num) {
		this.surplus_park_num = surplus_park_num;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getInit_price() {
		return init_price;
	}

	public void setInit_price(double init_price) {
		this.init_price = init_price;
	}

	public double getPer_hour_price() {
		return per_hour_price;
	}

	public void setPer_hour_price(double per_hour_price) {
		this.per_hour_price = per_hour_price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
