package znck.bean.weixin.message;

public class QRCodeEvent extends BaseEvent {

	
	/**
	 * 事件key值
	 */
	private String EventKey;
	
	/**
	 * 二维码值,用来换取二维码
	 */
	private String Ticket;

	public String getEventKey() {
		return EventKey;
	}

	public void setEventKey(String eventKey) {
		EventKey = eventKey;
	}

	public String getTicket() {
		return Ticket;
	}

	public void setTicket(String ticket) {
		Ticket = ticket;
	}
	
	
}
