package znck.bean.weixin.message;

public class SubscribeEvent extends BaseEvent {
	
	/**
	 * 事件key值
	 */
	private String EventKey;    //微信关注事件也有EventKey值，不过EventKey值为空，官方文档可能有点歧义，以实际测试为准

	public String getEventKey() {
		return EventKey;
	}

	public void setEventKey(String eventKey) {
		EventKey = eventKey;
	}
	
	
}
