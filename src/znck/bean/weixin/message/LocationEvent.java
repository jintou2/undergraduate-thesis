package znck.bean.weixin.message;

public class LocationEvent extends BaseEvent {

	/**
	 * 经度
	 */
	private String Longitude;
	
	/**
	 * 纬度
	 */
	private String Latitude;
	
	/**
	 * 地理位置精度
	 */
	private String Precision;

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getPrecision() {
		return Precision;
	}

	public void setPrecision(String precision) {
		Precision = precision;
	}
	
	
}
