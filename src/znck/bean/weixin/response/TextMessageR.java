package znck.bean.weixin.response;

public class TextMessageR extends BaseMessageR {

	/**
	 * 回复的消息内容
	 */
	private String Content;

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}
	
	
}
