package znck.bean.weixin.menu;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author wxj233
 */
public class SynthesisMenu {

	/**
	 * 菜单类型
	 */
	private String type;
	
	/**
	 * 菜单名称
	 */
	private String name;
	
	/**
	 * 按钮键值
	 */
	private String key;
	
	/**
	 * 按钮url
	 */
	private String url;
	
	/**
	 * 子菜单
	 */
	private List<SynthesisMenu> sub_button = new ArrayList<>();

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<SynthesisMenu> getSub_button() {
		return sub_button;
	}

	public void setSub_button(List<SynthesisMenu> sub_button) {
		this.sub_button = sub_button;
	}
	
	
}
