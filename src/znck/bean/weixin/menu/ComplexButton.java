package znck.bean.weixin.menu;

import java.util.ArrayList;

public class ComplexButton extends Button {

	/**
	 * 复合菜单
	 */
	private ArrayList<Button> sub_button = new ArrayList<Button>();

	public ArrayList<Button> getSub_button() {
		return sub_button;
	}
 
	public void setSub_button(ArrayList<Button> sub_button) {
		this.sub_button = sub_button;
	}


}
