package znck.bean.weixin.menu;

public class ClickButton extends Button {

	/*
	 * 类型，此处为click
	 */
	private String type;
	
	/*
	 * 事件key值
	 */
	private String key;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	
}
