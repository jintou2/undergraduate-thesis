package znck.bean.weixin.menu;

public class ViewButton extends Button {

	/*
	 * 类型，此处为view
	 */
	private String type;
	
	/*
	 * url
	 */
	private String url;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
