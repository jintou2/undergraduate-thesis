package znck.hb.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 标识属性
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	/**
	 * 微信openID
	 */
	private String openID;
	
	/**
	 * 用户邮箱
	 */
	private String email;
	
	/**
	 * 账户密码
	 */
	private String password;
	
	/**
	 * 用户姓名
	 */
	private String name;
	
	/**
	 * 电话
	 */
	private String phone;
	
	/**
	 * 用户车辆
	 */
	@OneToMany(targetEntity=Car.class, mappedBy="user", fetch=FetchType.LAZY, cascade={CascadeType.REMOVE})
	private List<Car> cars = new ArrayList<>();
	
	/**
	 * 停车信息/订单记录
	 */
	@OneToMany(targetEntity=Park.class, mappedBy="user", fetch=FetchType.LAZY, cascade={CascadeType.REMOVE})
	private List<Park> parks = new ArrayList<>();
	
	/**
	 * 停车场
	 */
	@OneToMany(targetEntity=ParkingLot.class, mappedBy="user", fetch=FetchType.LAZY, cascade={CascadeType.REMOVE})
	private List<ParkingLot> parking_lots = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOpenID() {
		return openID;
	}

	public void setOpenID(String openID) {
		this.openID = openID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public List<Park> getParks() {
		return parks;
	}

	public void setParks(List<Park> parks) {
		this.parks = parks;
	}

	public List<ParkingLot> getParking_lots() {
		return parking_lots;
	}

	public void setParking_lots(List<ParkingLot> parking_lots) {
		this.parking_lots = parking_lots;
	}
	
	
	
}
