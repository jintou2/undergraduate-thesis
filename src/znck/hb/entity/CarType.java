package znck.hb.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="cartype")
public class CarType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 标识属性
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	/**
	 * 类型
	 */
	private String type;
	
	/**
	 * 停车场
	 */
	@ManyToMany(targetEntity=ParkingLot.class, mappedBy="cartypes", fetch=FetchType.LAZY) 
	private List<ParkingLot> parking_lots = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<ParkingLot> getParking_lots() {
		return parking_lots;
	}

	public void setParking_lots(List<ParkingLot> parking_lots) {
		this.parking_lots = parking_lots;
	}
	
	
}
