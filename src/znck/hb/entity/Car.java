package znck.hb.entity;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="car")
public class Car implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 标识属性
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	/**
	 * 车辆类型
	 */
	@OneToOne(targetEntity=CarType.class, fetch=FetchType.LAZY)
	@JoinColumn(name="type_id" , referencedColumnName="id" , nullable=false)
	private CarType type;
	
	/**
	 * 用户
	 */
	@ManyToOne(targetEntity=User.class, fetch=FetchType.LAZY)
	@JoinColumn(name="user_id" , referencedColumnName="id" , nullable=false)
	private User user;
	
	/**
	 * 是否审核
	 */
	private int whethercheck;
	
	/**
	 * 车辆牌照
	 */
	private String number;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public CarType getType() {
		return type;
	}

	public void setType(CarType type) {
		this.type = type;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getWhethercheck() {
		return whethercheck;
	}

	public void setWhethercheck(int whethercheck) {
		this.whethercheck = whethercheck;
	}

}
