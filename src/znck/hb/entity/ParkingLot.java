package znck.hb.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="parkinglot")
public class ParkingLot implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 标识属性
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	/**
	 * 用户
	 */
	@ManyToOne(targetEntity=User.class, fetch=FetchType.LAZY)
	@JoinColumn(name="user_id" , referencedColumnName="id" , nullable=false)
	private User user;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 经度
	 */
	private double longitude;
	
	/**
	 * 纬度
	 */
	private double latitude;
	
	/**
	 * 区域编号
	 */
	private int areaId;
	
	/**
	 * 车场地址
	 */
	private String address;
	
	/**
	 * 停车位
	 */
	private int park_number;
	
	/**
	 * 可停放车辆类型
	 */
	@ManyToMany(targetEntity=CarType.class, fetch=FetchType.LAZY)
	@JoinTable(name="park_cartype",
	joinColumns={@JoinColumn(name="parkid")},
	inverseJoinColumns={@JoinColumn(name="car_typeid")})
	private List<CarType> cartypes = new ArrayList<>();
	
	/**
	 * 停车信息
	 */
	@OneToMany(targetEntity=Park.class, mappedBy="parking_lot", fetch=FetchType.LAZY)
	private List<Park> parks = new ArrayList<>();
	
	/**
	 * 剩余停车位
	 */
	private int surplus_park_num;
	
	/**
	 * 车场照片
	 */
	private String image;
	
	/**
	 * 证明材料
	 */
	private String support_doc_image;
	
	/**
	 * 状态
	 */
	private int status;
	
	/**
	 * 起始价
	 */
	private double init_price;
	
	/**
	 * 单价
	 */
	private double per_hour_price;
	
	/**
	 * 停车场简介
	 */
	private String description;

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getPark_number() {
		return park_number;
	}

	public void setPark_number(int park_number) {
		this.park_number = park_number;
	}

	public List<CarType> getCartypes() {
		return cartypes;
	}

	public void setCartypes(List<CarType> cartypes) {
		this.cartypes = cartypes;
	}

	public List<Park> getParks() {
		return parks;
	}

	public void setParks(List<Park> parks) {
		this.parks = parks;
	}

	public int getSurplus_park_num() {
		return surplus_park_num;
	}

	public void setSurplus_park_num(int surplus_park_num) {
		this.surplus_park_num = surplus_park_num;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getSupport_doc_image() {
		return support_doc_image;
	}

	public void setSupport_doc_image(String support_doc_image) {
		this.support_doc_image = support_doc_image;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getInit_price() {
		return init_price;
	}

	public void setInit_price(double init_price) {
		this.init_price = init_price;
	}

	public double getPer_hour_price() {
		return per_hour_price;
	}

	public void setPer_hour_price(double per_hour_price) {
		this.per_hour_price = per_hour_price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
