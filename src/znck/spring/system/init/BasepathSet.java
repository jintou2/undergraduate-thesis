package znck.spring.system.init;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import znck.spring.service.util.SystemService;

@Service
public class BasepathSet {

	@Autowired
	SystemService systemService;
	@Autowired
	ServletContext servletContext;
	
	/*
	 * 设置basepath
	 */
	@PostConstruct
	void basepathset() {
		servletContext.setAttribute("basepath", systemService.getProperty_key("basepath"));
	}
}
