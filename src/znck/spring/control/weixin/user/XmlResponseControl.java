package znck.spring.control.weixin.user;

import java.io.IOException;




import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import znck.spring.service.weixin.XmlResponseService;
import znck.spring.service.weixin.XmlService;

@Controller
@RequestMapping
public class XmlResponseControl {

	@Autowired
	XmlService xmlService;
	@Autowired
	XmlResponseService xmlResponseService;
	
	/**
	 * 用户XML请求数据
	 * @return  返回欢迎用户关注信息
	 * @throws UnsupportedEncodingException 
	 * @throws IOException 
	 * @throws DocumentException 
	 * 
	 * 注意:
	 * "application/json" json格式数据
	 * "application/xml" xml类型数据
	 * "application/x-www-form-urlencoded" 基本类型  jquery默认就是采用的这种
	 * "multipart/form-data" 文件上传
	 * "text/xml"  和application/xml采用的默认编码格式不一样
	 * "text/html"  一般的html
	 */
	@RequestMapping(value = "/weixin/validation" , method = RequestMethod.POST , produces = "text/html;charset=UTF-8")
	@ResponseBody
	String UserRequest(HttpServletRequest request) throws UnsupportedEncodingException, IOException {
		InputStreamReader c_in = new InputStreamReader(request.getInputStream(),"UTF-8");    //转换为字符流
		Document document = xmlService.ObtainXmlDoc(c_in);
		c_in.close();
		return xmlResponseService.XmlResponse(document);
	}
	

}
