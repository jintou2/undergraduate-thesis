package znck.spring.control.weixin.menu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;
import znck.bean.weixin.menu.SynthesisMenu;
import znck.spring.service.util.SystemService;
import znck.spring.service.util.URLService;
import znck.spring.service.weixin.MenuService;

@Controller
@RequestMapping(value = "/weixin/menu")
public class MenuControl {

	@Autowired
	MenuService menuService;
	@Autowired
	SystemService systemService;
	@Autowired
	URLService urlService;
	
	/**
	 * 菜单编辑
	 * @param menujson
	 * @return
	 */
	@RequestMapping(value = "/modify" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	String menuModify(String menujson) {
		JSONObject jsonObj = new JSONObject();
		//System.out.println(menujson);
		return jsonObj.accumulate("result", menuService.menuEdit(menujson)).toString();
		//return jsonObj.accumulate("result", menuService.menuModify(menujson)).toString();
	}
	
	/**
	 * 菜单删除
	 * @return
	 */
	@RequestMapping(value = "/delete" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	String menuDelete() {
		JSONObject jsonObj = new JSONObject();
		return jsonObj.accumulate("result", menuService.menuDelete()).toString();
	}
	
	/**
	 * 菜单查询
	 * @return
	 */
	@RequestMapping(value = "/query", produces = "text/html;charset=UTF-8")
	@ResponseBody
	String menuQuery(Model model) {
		return menuService.menuQuery().toString();
	}
	
	/**
	 * 微信菜单添加页面
	 */
	@RequestMapping(value = "/add")
	String meneAdd() {
		return "addMune";
	}
	
	/**
	 * @author wxj233
	 * 菜单页面
	 */
	@RequestMapping(value = "/viewMenu", produces = "text/html;charset=UTF-8")
	String viewMenu(Model model) {
		Map<String, Class<SynthesisMenu>> classMap = new HashMap<String, Class<SynthesisMenu>>();  
		classMap.put( "sub_button", SynthesisMenu.class);
		List<SynthesisMenu> SynthesisMenus = systemService.JSONArrayToListbean(SynthesisMenu.class
		, menuService.menuQuery().getJSONObject("menu").getJSONArray("button"), classMap);
		
		model.addAttribute("SynthesisMenus", menuService.getCallBackUrl(SynthesisMenus));
		
		return "viewMenu";
	}
	
	
	
}
