package znck.spring.control.weixin.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import znck.spring.service.util.SystemService;
import znck.spring.service.weixin.ValidationService;

@Controller
@RequestMapping
public class TokenValidationControl {

	@Autowired
	ValidationService validationService;
	@Autowired
	SystemService systemSetService;
	
	/**
	 * 微信token验证程序
	 * @param signature 微信加密签名
	 * @param timestamp 时间戳
	 * @param nonce 随机数
	 * @param echostr 随机字符串
	 */
	@RequestMapping(value = "/weixin/validation" , method = RequestMethod.GET)
	@ResponseBody
	String Validation(String signature, String timestamp, String nonce, String echostr) {
		String token = systemSetService.getProperty_key("token");
		if(validationService.checkSignature(signature, timestamp, nonce, token)) {
			return echostr;
			
		}else {
			return "The request does not come from WeiXin!";
		}
	}
	
}
