package znck.spring.control.parklots;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ComplaintControl {
	
	/**
	 * 投诉页面
	 */
	@RequestMapping(value = "/complaint")
	String addCarParklot() {
		return "complaint";
	}
	
	/**
	 * 关于我们
	 */
	 @RequestMapping(value = "/about")
	 String aboutUs(){
		 return "about";
	 }
	 
	 /**
	  * 联系我们
	  */
	 @RequestMapping(value = "/contact")
	 String contact(){
		 return "contact";
	 }
}
