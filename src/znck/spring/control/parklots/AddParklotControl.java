package znck.spring.control.parklots;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;
import znck.bean.dto.Rparkinglot;
import znck.hb.entity.ParkingLot;
import znck.spring.service.parklots.AddParklotService;
import znck.spring.service.parklots.ParkingLotService;

@Controller
public class AddParklotControl {
	
	@Autowired
	AddParklotService addParklotService;
	
	@Autowired
	ParkingLotService parkingLotService;
	
	/**
	 * 拦截器
	 */
	@RequestMapping(value = "/filter")
	String filter() {
		return "login";
	}

	/**
	 * 添加停车场页面
	 */
	@RequestMapping(value = "/addParklot")
	String addCarParklot() {
		return "addCarPark";
	}
	
	/**
	 * 添加停车场
	 */
	@RequestMapping(value = "/addParklot/addPark", produces = "text/html;charset=UTF-8")
	@ResponseBody
	String addParklot(ParkingLot parklot,String cartype,HttpSession httpsession) {
		System.out.print("收到请求");
		System.out.print(cartype);
		JSONObject jsonObj = new JSONObject();
		if(addParklotService.parkingLotAdd(httpsession, parklot,cartype)) {
			jsonObj.accumulate("result", true);
		}else {
			jsonObj.accumulate("result", false);
		}
		return jsonObj.toString();
	} 
	
	/**
	 * 用户查询自己的停车场
	 */
	@RequestMapping(value = "/addParklot/queryParklot", produces = "text/html;charset=UTF-8")
	String queryParklot(HttpSession httpsession,Model model) {
		List<Rparkinglot> usersParkinglots = addParklotService.usersParkinglots(httpsession);
		model.addAttribute("usersParkinglots",usersParkinglots);
		return "userParklot";
	}
	
	/**
	 * 车位市场
	 */
	@RequestMapping(value = "/addParklot/parkMarket")
	String parkinglots(Model model) {
		List<Rparkinglot> parkinglots = addParklotService.queryParkinglots(); 
		model.addAttribute("parkinglots", parkinglots);
		return "parkmarket";
	}
	
	/**
	 * 用户自己的停车场，根据id查询停车场
	 * @param parklotId 路径变量车场id
	 */
	@RequestMapping(value = "/userParklot/profile/{parklotId}")
	String parklotprofile(@PathVariable int parklotId, Model model) {
		model.addAttribute("profile", parkingLotService.parklotprofile(parklotId));
		return "mudifyUserParklots";
	}
	
	/**
	 * 用户删除停车场
	 */
	@RequestMapping(value = "/userParklot/deleteParklot/{parklotId}",produces = "text/html;charset=UTF-8")
	String deleteParklot(@PathVariable int parklotId,HttpSession httpsession,Model model) {
		model.addAttribute("usersParkinglots", addParklotService.deleteParklots(httpsession, parklotId));
		return "userParklot";
	}
	
	/**
	 * 更新停车场页面
	 */
	@RequestMapping(value = "/userParklot/updateParklot/{parklotId}")
	String updateParklot(@PathVariable int parklotId, Model model) {
		model.addAttribute("profile", parkingLotService.parklotprofile(parklotId));
		return "updateParklot";
	}
	
	/**
	 * 更新停车场
	 */
	@RequestMapping(value = "/userParklot/updateParklot", produces = "text/html;charset=UTF-8")
	@ResponseBody
	String update(ParkingLot parklot,HttpSession httpsession,Model model) {
		System.out.print("收到请求"+parklot.getAddress());
		List<Rparkinglot> userParkinglotsList = addParklotService.updateParklots(httpsession,parklot);
		model.addAttribute("userParkinglotsList", userParkinglotsList);
		JSONObject jsonObj = new JSONObject();
		jsonObj.accumulate("result",true);
		return jsonObj.toString();
	}
}
