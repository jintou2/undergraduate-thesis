package znck.spring.control.parklots;

import javax.servlet.http.HttpSession;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;
import znck.bean.dto.Location;
import znck.hb.entity.Park;
import znck.spring.service.parklots.ParkingLotService;
import znck.spring.service.util.SystemService;

@Controller
public class ParkLotsControl {

	@Autowired
	ParkingLotService parkingLotService;
	@Autowired 
	SystemService systemService;
	
	/**
	 * 停车场页面
	 */
	@RequestMapping(value = "/parklots")
	String parklots(Model model) {
		//BasepathSet basepathSet = SpringContextHolder.getBean(BasepathSet.class);
		//System.out.println("BeanName:"+basepathSet.getClass().getName());
		//System.out.println("beanNumber"+SpringContextHolder.getApplicationContext().getBeanDefinitionCount());
		//UserServiceImp userServiceImp = (UserServiceImp) SpringContextHolder.getBean("userServiceImp");
		//UserService userServiceImp = (UserService) SpringContextHolder.getBean("userServiceImp");
		//System.out.println("BeanName:"+userServiceImp.getClass().getName());
		
		return "parklots";
	}
	
	/**
	 * 车位订购
	 * @param parklotId 停车场id
	 * 日期转换问题？使用日期格式转换器
	 */
	@RequestMapping(value = "/parklot/order")
	@ResponseBody
	String parklotOder(int parklotId, Park park, HttpSession httpsession) {
		//System.out.println("预计到达时间："+park.getEstimateArrival());
		JSONObject jsonObj = new JSONObject();
		int status = parkingLotService.parkOder(parklotId, park, httpsession);
			jsonObj.accumulate("status", status);
			return jsonObj.toString();
	}
	
	/**
	 * 附近车场搜索,暂时默认少于20个车场
	 * @param location 搜索位置的经纬度
	 */
	@RequestMapping(value = "/parklot/search", produces = "text/html;charset=UTF-8")
	@ResponseBody
	String ParklotSearch(Location location) {
		//System.out.println(parkingLotService.ParklotsSearch(location, 20).size());
		return systemService.JavabeanTojsonarray(parkingLotService.ParklotsSearch(location, 20));
	}
	
	/**
	 * 车场详细信息
	 * @param parklotId 路径变量车场id
	 */
	@RequestMapping(value = "/parklot/profile/{parklotId}")
	String parklotprofile(@PathVariable int parklotId, Model model) {  
		model.addAttribute("profile", parkingLotService.parklotprofile(parklotId));
		return "parklotprofile";
	}
	
	/**
	 * 订单取消
	 * @param parkId 订单id
	 */
	@RequestMapping(value = "/parklot/order/cancel")
	@ResponseBody
	String parklotCancel(int parkId, HttpSession httpsession) {
		JSONObject jsonObj = new JSONObject();
		if(parkingLotService.parkCancel(parkId, httpsession)) {
			jsonObj.accumulate("result", true);
		}else {
			jsonObj.accumulate("result", false);
		}
		return jsonObj.toString();
	}
	
	/**
	 * 车辆入库
	 * @param parklotid 车库id
	 * @param carnumber 车牌号
	 */
	@RequestMapping(value = "/parklot/instorage/{parklotid}/{carnumber}")
	String InStorageCon(@PathVariable int parklotid, @PathVariable String carnumber) {
		JSONObject jsonObj = new JSONObject();
		if(parkingLotService.InStorage(parklotid, carnumber)) {
			jsonObj.accumulate("result", true);
		}else {
			jsonObj.accumulate("result", false);
		}
		return jsonObj.toString();
	}
	
	/**
	 * 车辆出库
	 * @param parklotid 车库id
	 * @param carnumber 车牌号
	 */
	@RequestMapping(value = "/parklot/outstorage/{parklotid}/{carnumber}")
	String OutStorageCon(@PathVariable int parklotid, @PathVariable String carnumber) {
		JSONObject jsonObj = new JSONObject();
		if(parkingLotService.OutStorage(parklotid, carnumber)) {
			jsonObj.accumulate("result", true);
		}else {
			jsonObj.accumulate("result", false);
		}
		return jsonObj.toString();
	}
	
}
