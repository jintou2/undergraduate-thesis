package znck.spring.control.parklots;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import znck.bean.weixin.jssdk.JsSDK;
import znck.spring.service.parklots.UseSdkService;
import znck.spring.service.util.SystemService;

@Controller
public class UsesdkController {
	
	@Autowired
	UseSdkService useSdkService;
	
	@Autowired
	SystemService systemSetService;
	
	/**
	 * appID公众号唯一标识
	 * timestamp: , // 必填，生成签名的时间戳
	 * nonceStr: '', // 必填，生成签名的随机串
	 * signature: '',// 必填，签名
	 * jsApiList: [] // 必填，需要使用的JS接口列表
	 * URL:当前网页的url
	 */
	@RequestMapping(value = "/getJsSdk" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	String getJsSDK(Model model) {
		JsSDK sdk = useSdkService.formatQueryParaMap();
		System.out.print("********string1****" + systemSetService.JavabeanTojson(sdk));
		return systemSetService.JavabeanTojson(sdk);
	}
	
	/**
	 * 获取页面需要的配置信息的参数
	 * 
	 */
	@RequestMapping(value = "/getJsTicket" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Map<String, Object> getWeJsTicket(HttpServletRequest request, String url){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 获取微信signature
			// 取出ServletContext的某个属性
		    //1.首先获取到ServletContext
//		    ServletContext sin = Servlet.getServletConfig();  
//		    //2.取出属性
//		    String name = (String)servletContext.getAttribute("name");
//			map.put("appId", systemSetService.getProperty_key("appId"));
//			map.put("timestamp", sin.getTimestamp());
//			map.put("nonceStr", sin.getNonceStr());
//			map.put("signature", sin.getSignature());
		} catch (Exception e) {
			 System.out.print(e.getMessage());
		}
		return map;
	}
}
