package znck.spring.control.user;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;
import znck.hb.entity.User;
import znck.spring.service.user.UserService;
import znck.spring.service.util.SMSService;

@Controller
@RequestMapping(value = "/user")
public class UserControl {
	
	@Autowired
	SMSService sMSService;
	@Autowired
	UserService userService;
	
	/**
	 * 用户注册页面
	 */
	@RequestMapping(value = "/view/register")
	String UserRegisterView() {
		return "signin";
	}
	
	/**
	 * 短信发送
	 * 用户注册，短信发送
	 */
	@RequestMapping(value = "/verification/register", produces = "text/html;charset=UTF-8")
	@ResponseBody
	String SendVerificationCode(HttpSession httpsession, String phone) {
		JSONObject jsonObj = new JSONObject();
		if(sMSService.SMS_Send(phone, httpsession)) {
			jsonObj.accumulate("result", true);
		}else {
			jsonObj.accumulate("result", false);
		}
		return jsonObj.toString();
	}
	
	
	/**
	 * 用户登录页面
	 */
	@RequestMapping(value = "/view/login")
	String UserLoginView() {
		return "login";
	}
	
	/**
	 * 用户手机注册
	 */
	@RequestMapping(value = "/phone/register", produces = "text/html;charset=UTF-8")
	@ResponseBody
	String UserPhoneRegister(User user, String verification_code, HttpSession httpsession) {
		JSONObject jsonObj = new JSONObject();
		if(userService.phoneRegister(user, verification_code, httpsession)) {
			jsonObj.accumulate("result", true);
		}else {
			jsonObj.accumulate("result", false);
		}
		//System.out.print(jsonObj);
		return jsonObj.toString();
	}
	
	/**
	 * 手机用户登录
	 */
	@RequestMapping(value = "/phone/login", produces = "text/html;charset=UTF-8")
	@ResponseBody
	String UserPhoneLogin(User user, HttpSession httpsession) {
		//System.out.println(user.getPhone());
		JSONObject jsonObj = new JSONObject();
		if(userService.phoneLogin(user, httpsession)) {
			jsonObj.accumulate("result", true);
		}else {
			jsonObj.accumulate("result", false);
		}
		return jsonObj.toString();
	}
	
	/**
	 * 我的订单,包含车场信息
	 */
	@RequestMapping(value = "/myorder")
	String MyOrder(HttpSession httpsession, Model model) {
		model.addAttribute("parks", userService.selectParks(httpsession));
		return "myorder";
	}

}
