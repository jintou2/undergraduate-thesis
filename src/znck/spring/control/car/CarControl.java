package znck.spring.control.car;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;
import znck.hb.entity.Car;
import znck.spring.service.car.CarService;
import znck.spring.service.car.CarTypeService;

@Controller
@RequestMapping(value = "/car")
public class CarControl {

	@Autowired
	CarTypeService carTypeService;
	@Autowired
	CarService carService;
	/**
	 * 添加my car页面
	 */
	@RequestMapping(value = "/add", produces = "text/html;charset=UTF-8")
	String CarAdd(Model model) {
		model.addAttribute("cartypes", carTypeService.getAllCarType());
		return "addcar";
	}
	
	/**
	 * 添加车辆
	 */
	@RequestMapping(value = "/cardata/add")
	@ResponseBody
	String CardataAdd(HttpSession httpsession, int cartypeId, Car car) {
		JSONObject jsonObj = new JSONObject() ;
		if(carService.CarAdd(httpsession, cartypeId, car)) {
			jsonObj.accumulate("result", true);
		}else {
			jsonObj.accumulate("result", false);
		}
		return jsonObj.toString();
	}
	
}
