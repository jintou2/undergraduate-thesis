package znck.spring.serviceImp.car;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import znck.hb.entity.Car;
import znck.hb.entity.CarType;
import znck.hb.entity.User;
import znck.spring.dao.CarDao;
import znck.spring.service.car.CarService;

@Service
@Transactional
public class CarServiceImp implements CarService {

	@Autowired
	CarDao carDao;
	
	@Override
	public boolean CarAdd(HttpSession httpsession, int cartypeId, Car car) {
		// TODO Auto-generated method stub
		User user = (User) httpsession.getAttribute("user");
		CarType cartype = new CarType();
		cartype.setId(cartypeId);
		car.setUser(user);
		car.setType(cartype);
		
		carDao.save(car);
		
		return true;
	}

}
