package znck.spring.serviceImp.car;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import znck.bean.dto.Rcartype;
import znck.hb.entity.CarType;
import znck.spring.dao.CarTypeDao;
import znck.spring.service.car.CarTypeService;

@Service
@Transactional
public class CarTypeServiceImp implements CarTypeService {

	@Autowired
	CarTypeDao cartypeDao;
	
	@Override
	public List<Rcartype> getAllCarType() {
		// TODO Auto-generated method stub
		List<CarType> cartypes = cartypeDao.queryAllType();
		List<Rcartype> rcs = new ArrayList<>();
		 for(CarType ct : cartypes) {
			 Rcartype rca = new Rcartype();
			 rca.setId(ct.getId());
			 rca.setType(ct.getType());
			 
			 rcs.add(rca);
		 }
		
		return rcs;
	}

}
