package znck.spring.serviceImp.weixin;

import java.util.Date;

import org.dom4j.Document;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import znck.bean.constant.WeiXinMsgAndEvent;
import znck.bean.constant.WeixinMessage;
import znck.bean.weixin.message.SubscribeEvent;
import znck.bean.weixin.response.TextMessageR;
import znck.hb.entity.User;
import znck.spring.dao.UserDao;
import znck.spring.service.weixin.XmlResponseService;
import znck.spring.service.weixin.XmlService;

@Service
@Transactional
public class XmlResponseServiceImp implements XmlResponseService {

	@Autowired
	XmlService xmlService;
	@Autowired
	UserDao userDao;
	
	@Override
	public String XmlResponse(Document document) {
		// TODO Auto-generated method stub
		Element root = document.getRootElement();   //获取根元素
		Element MsgType = root.element("MsgType");       //这个需要一级一级的选elements只表示下一级子元素
		
		//文本消息
		if(MsgType.getText().equals(WeiXinMsgAndEvent.MsgTypeText)) {
			//do not
		}
		//语音消息
		else if(MsgType.getText().equals(WeiXinMsgAndEvent.MsgTypeVoice)) {
			//do not
		}
		//事件推送
		else if(MsgType.getText().equals(WeiXinMsgAndEvent.MsgTypeEvent)) {
			//关注事件
			Element EventType = root.element("Event");
			if(EventType.getText().equals(WeiXinMsgAndEvent.EventTypeSubscribe)) {
				return Subscribe(document);  //完成微信注册
			}
			//取消关注事件
			else if(EventType.getText().equals(WeiXinMsgAndEvent.EventTypeUnsubscribe)) {
				//do not
			}
			//扫描带参二维码事件
			else if(EventType.getText().equals(WeiXinMsgAndEvent.EventTypeSCAN)) {
				//do not
			}
			//地理位置事件
			else if(EventType.getText().equals(WeiXinMsgAndEvent.EventTypeLOCATION)) {
				//do not
			}
			//其他事件
			else {
				//do not
			}
		}
		//其他消息
		else {
			//do not
		}
		
		return defaultResponse(document);
	}

	@Override
	public String Subscribe(Document document) {
		// TODO Auto-generated method stub
		
		SubscribeEvent subscribeEvent = xmlService.getBean(SubscribeEvent.class, document);
		TextMessageR textMessageR = new TextMessageR();
	    textMessageR.setContent(WeixinMessage.welcome);
		textMessageR.setCreateTime(new Date().getTime());
		textMessageR.setFromUserName(subscribeEvent.getToUserName());
		textMessageR.setMsgType("text");
		textMessageR.setToUserName(subscribeEvent.getFromUserName());
		//System.out.println(xmlService.JavabeanToXml(textMessageR));
		
		User user = new User();
		user.setOpenID(subscribeEvent.getFromUserName());
		userDao.WeiXinUserAdd(user);
		
		return xmlService.JavabeanToXml(textMessageR);
	}

	@Override
	public String defaultResponse(Document document) {
		// TODO Auto-generated method stub
		Element root = document.getRootElement();   //获取根元素
		Element ToUserName = root.element("ToUserName");
		Element FromUserName = root.element("FromUserName");
		TextMessageR textMessageR = new TextMessageR();
	    textMessageR.setContent(WeixinMessage.noFunction);
		textMessageR.setCreateTime(new Date().getTime());
		textMessageR.setFromUserName(ToUserName.getText());
		textMessageR.setMsgType("text");
		textMessageR.setToUserName(FromUserName.getText());
		return xmlService.JavabeanToXml(textMessageR);
	}
	
	
	
	
	

}
