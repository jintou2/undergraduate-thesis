package znck.spring.serviceImp.weixin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;


import javax.servlet.ServletContext;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import znck.bean.weixin.menu.SynthesisMenu;
import znck.spring.service.util.SystemService;
import znck.spring.service.util.URLService;
import znck.spring.service.weixin.MenuService;

@Service
public class MenuServiceImp implements MenuService {

	@Autowired
	SystemService systemService;
	@Autowired
	ServletContext servletContext;
	@Autowired
	URLService urlService;

	@Override
	public boolean menuModify(String jsonmenu) {
		// TODO Auto-generated method stub
		String url = systemService.getProperty_key("MenuCreatUrl")+servletContext.getAttribute("access_token");
		//System.out.println(servletContext.getAttribute("access_token"));
		StringBuffer buffer = systemService.HttpsSend(url, "POST", jsonmenu);
		JSONObject jsonObj = JSONObject.fromObject(buffer.toString());
		if(jsonObj.getInt("errcode")==0) {
			return true;
		}else {
			return false;
		}
	}
	
	@Override
	public boolean menuEdit(String jsonmenu) {
		// TODO Auto-generated method stub
		JSONObject menu = JSONObject.fromObject(jsonmenu);
		JSONArray buttons = menu.getJSONArray("button");
		
		String AuthorizationUrl = systemService.getProperty_key("Authorization");
		String redirect_uri = systemService.getProperty_key("redirect_uri");
		for(int i = 0; i<buttons.size(); i++) {
			JSONObject button = buttons.getJSONObject(i);
			
			//遍历循环替换
			if(button.containsKey("type")) {
				String type = button.getString("type");
				if("view".equals(type)) {
					String tempAuthorizationUrl = AuthorizationUrl;   //复制对象，String非引用
					try {
						tempAuthorizationUrl = tempAuthorizationUrl.replace(redirect_uri, URLEncoder.encode(button.getString("url"),"UTF-8"));
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					button.element("url", tempAuthorizationUrl);
				}
			}else {
				JSONArray secendLevelMenu = button.getJSONArray("sub_button");
				for(int j = 0; j < secendLevelMenu.size(); j++) {
					JSONObject secendLevelbutton = secendLevelMenu.getJSONObject(j);
					if("view".equals(secendLevelbutton.getString("type"))) {
						String tempAuthorizationUrl = AuthorizationUrl;   //复制对象，String非引用
						try {
							tempAuthorizationUrl = tempAuthorizationUrl.replace(redirect_uri, URLEncoder.encode(secendLevelbutton.getString("url"),"UTF-8"));
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						secendLevelbutton.element("url", tempAuthorizationUrl);
					}
				}
			}
			
		}
		
		jsonmenu = menu.toString();
		System.out.println(jsonmenu);
		String url = systemService.getProperty_key("MenuCreatUrl")+servletContext.getAttribute("access_token");
		//System.out.println(servletContext.getAttribute("access_token"));
		StringBuffer buffer = systemService.HttpsSend(url, "POST", jsonmenu);
		JSONObject jsonObj = JSONObject.fromObject(buffer.toString());
		if(jsonObj.getInt("errcode")==0) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public JSONObject menuQuery() {
		// TODO Auto-generated method stub
		String url = systemService.getProperty_key("MenuQueryUrl")+servletContext.getAttribute("access_token");
		StringBuffer buffer = systemService.HttpsSend(url, "GET", null);
		JSONObject jsonObj = JSONObject.fromObject(buffer.toString());
		return jsonObj;
	}

	@Override
	public boolean menuDelete() {
		// TODO Auto-generated method stub
		String url = systemService.getProperty_key("MenuDeleteUrl")+servletContext.getAttribute("access_token");
		StringBuffer buffer = systemService.HttpsSend(url, "GET", null);
		JSONObject jsonObj = JSONObject.fromObject(buffer.toString());
		if(jsonObj.getInt("errcode")==0) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public List<SynthesisMenu> getCallBackUrl(List<SynthesisMenu> SynthesisMenus) {
		// TODO Auto-generated method stub
		try {
		for(SynthesisMenu sy : SynthesisMenus) {
			if("view".equals(sy.getType())) {
					sy.setUrl(URLDecoder.decode(urlService.getUrlParameter(sy.getUrl(),"redirect_uri"),"UTF-8"));
			}
			for(SynthesisMenu sub : sy.getSub_button()) {
				if("view".equals(sub.getType())) {
					sub.setUrl(URLDecoder.decode(urlService.getUrlParameter(sub.getUrl(),"redirect_uri"),"UTF-8"));
				}
			}
		}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SynthesisMenus;
	}



}
