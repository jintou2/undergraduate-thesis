package znck.spring.serviceImp.weixin;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.springframework.stereotype.Service;

import znck.spring.service.weixin.ValidationService;

@Service
public class ValidationServiceImp implements ValidationService {

	@Override
	public String byteToStr(byte[] byteArray) {
		// TODO Auto-generated method stub
		String strDigest = "";
        for (int i = 0; i < byteArray.length; i++) {
            strDigest += byteToHexStr(byteArray[i]);
        }
        return strDigest;
	}

	@Override
	public String byteToHexStr(byte mByte) {
		// TODO Auto-generated method stub
		char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        char[] tempArr = new char[2];
        tempArr[0] = Digit[(mByte >>> 4) & 0X0F];
        tempArr[1] = Digit[mByte & 0X0F];
        String s = new String(tempArr);
        return s;
	}

	@Override
	public boolean checkSignature(String signature, String timestamp, String nonce, String token) {
		// TODO Auto-generated method stub
		try {
			System.out.print(token);
		String[] arr = new String[] { token, timestamp, nonce };
		//System.out.print(arr);
		Arrays.sort(arr);
		String content = arr[0].concat(arr[1].concat(arr[2]));
		
		String tmpStr = null;
		
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			// 将三个参数字符串拼接成一个字符串进行sha1加密
            byte[] digest = md.digest(content.toString().getBytes());
            tmpStr = byteToStr(digest);
            return  tmpStr != null ? tmpStr.equals(signature.toUpperCase()) : false;
		}catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }catch (NullPointerException e) {
        	e.printStackTrace();
        	
        }
		return false;
		
	}

}
