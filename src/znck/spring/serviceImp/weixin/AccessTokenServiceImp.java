package znck.spring.serviceImp.weixin;
import javax.servlet.ServletContext;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;
import znck.spring.service.util.SystemService;
import znck.spring.service.weixin.AccessTokenService;
import znck.spring.serviceImp.util.SyLogImp;
@Service
public class AccessTokenServiceImp implements AccessTokenService {

	@Autowired
	SystemService systemService;
	@Autowired
	ServletContext servletContext;
	@Autowired
	SyLogImp syLogImp;
	
	@Override
	public String getAccessToken() {
		// TODO Auto-generated method stub
		String url = systemService.getProperty_key("AccessTokenUrl");
		StringBuffer buffer = systemService.HttpsSend(url, "GET", null);
		JSONObject jsonObj = JSONObject.fromObject(buffer.toString());
		String access_token = jsonObj.getString("access_token");
		servletContext.setAttribute("access_token", access_token);
		syLogImp.debug(AccessTokenServiceImp.class, "获取到微信access_token: "+access_token);
		//System.out.println("获取到微信access_token"+access_token);
		return access_token;
	}

}
