package znck.spring.serviceImp.weixin;

import java.io.InputStream;

import java.io.InputStreamReader;
import java.io.Writer;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;

import znck.spring.service.weixin.XmlService;

@Service
public class XmlServiceImp implements XmlService {

	@Override
	public Document ObtainXmlDoc(InputStreamReader in) {
		// TODO Auto-generated method stub
		SAXReader reader = new SAXReader();     //saxreader创建SAX解析事件dom4j树
		Document XMLdocument = null;
		try {
			XMLdocument = reader.read(in);  //读取给定流使用SAX文档
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
		return XMLdocument;
	}

	@Override
	public Document ObtainXmlDoc(InputStream in) {
		// TODO Auto-generated method stub
		SAXReader reader = new SAXReader();     //saxreader创建SAX解析事件dom4j树
		Document XMLdocument = null;
		try {
			XMLdocument = reader.read(in);  //读取给定流使用SAX文档
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
		return XMLdocument;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getBean(Class<T> type, InputStreamReader in) {
		// TODO Auto-generated method stub
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("xml", type);
		T obj = (T)xstream.fromXML(in);
		return obj;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getBean(Class<T> type, InputStream in) {
		// TODO Auto-generated method stub
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("xml", type);
		T obj = (T)xstream.fromXML(in);
		return obj;
	}

	@Override
	public String JavabeanToXml(Object obj) {
		// TODO Auto-generated method stub
		//XStream xstream = new XStream(new DomDriver());
		XStream xstream = getexpandXStream();
		xstream.alias("xml", obj.getClass());
		return xstream.toXML(obj);
	}

	@Override
	public Document JavabeanToDocument(Object obj) {
		// TODO Auto-generated method stub
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("xml", obj.getClass());
		Document XMLdocument = null;
		try {
			XMLdocument = DocumentHelper.parseText(xstream.toXML(obj));
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return XMLdocument;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getBean(Class<T> type, Document doc) {
		// TODO Auto-generated method stub
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("xml", type);
		//System.out.println(doc.asXML());
		T obj = (T)xstream.fromXML(doc.asXML());
		return obj;
	}
	
	/**
	 * 拓展XStream使其支持<![CDATA[content]]>
	 */
	@SuppressWarnings("rawtypes")
	public XStream getexpandXStream() {
		XStream xStream = new XStream(new XppDriver() {
			 public HierarchicalStreamWriter createWriter(Writer out) {
				 return new PrettyPrintWriter(out) {  
		                // 对所有xml节点的转换都增加CDATA标记  
		                boolean cdata = true;
		                String str = "";
		                public void startNode(String name, Class clazz) { 
		                	str = name;
		                    super.startNode(name, clazz);  
		                }  
		                protected void writeText(QuickWriter writer, String text) {  
		                    if (cdata&&!str.equals("CreateTime")) {  
		                        writer.write("<![CDATA["); 
		                        writer.write(text); 
		                        writer.write("]]>");  
		                    } else {  
		                        writer.write(text);  
		                    }  
		                }  
		            }; 
			 }
		});
		return xStream;
	}

}
