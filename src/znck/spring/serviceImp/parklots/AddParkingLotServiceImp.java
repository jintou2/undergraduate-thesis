package znck.spring.serviceImp.parklots;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import znck.hb.entity.ParkingLot;
import znck.bean.dto.Rparkinglot;
import znck.hb.entity.CarType;
import znck.hb.entity.User;
import znck.spring.dao.AddParklotDao;
import znck.spring.dao.CarTypeDao;
import znck.spring.dao.ParkDao;
import znck.spring.dao.UserDao;
import znck.spring.service.parklots.AddParklotService;

@Service
@Transactional
public class AddParkingLotServiceImp implements AddParklotService{
	
	@Autowired
	AddParklotDao addParklotDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	ParkDao parkdao;
	
	@Autowired
	CarTypeDao cartypeDao;

	@Override
	public boolean parkingLotAdd(HttpSession httpsession,ParkingLot parkinglot,String cartype){
		// TODO Auto-generated method stub
		User user = (User) httpsession.getAttribute("user");
		parkinglot.setUser(user);
		addParklotDao.save(parkinglot);
		CarType cartypes = new CarType();
		cartypes.setId(user.getId());
		cartypes.setType(cartype);
		cartypeDao.save(cartypes);
		return true;
	}

	@Override
	public List<Rparkinglot> queryParkinglots() {
		// TODO Auto-generated method stub
		List<ParkingLot> parkingLots = addParklotDao.queryParkingLots();
		List<Rparkinglot> rps = new ArrayList<Rparkinglot>();
		for(ParkingLot par : parkingLots) {
			Rparkinglot rp = new Rparkinglot();
		rp.setDescription(par.getDescription());
		rp.setId(par.getId());
		rp.setImage(par.getImage());
		rp.setInit_price(par.getInit_price());
		rp.setLatitude(par.getLatitude());
		rp.setLongitude(par.getLongitude());
		rp.setName(par.getName());
		rp.setPark_number(par.getPark_number());
		rp.setPer_hour_price(par.getPer_hour_price());
		rp.setSurplus_park_num(par.getSurplus_park_num());
		rp.setStatus(par.getStatus());
		
		rps.add(rp);
		}
		if(!parkingLots.isEmpty()) {
			return rps;
		}else {
			return null;
		}
	}
	
	@Override
	public List<Rparkinglot> usersParkinglots(HttpSession httpsession) {
		// TODO Auto-generated method stub
		User user = (User) httpsession.getAttribute("user");
		int id = user.getId(); 
		userDao.update(user);
//	    List<ParkingLot> parkinglots = user.getParking_lots();
		System.out.print("user.getId() 是"+user.getId());
		List<ParkingLot> parkinglots = addParklotDao.parkList(id);
		for(ParkingLot park :parkinglots) {
			System.out.print("park name 是"+park.getName());
		}
		List<Rparkinglot> rps = new ArrayList<Rparkinglot>();
		for(ParkingLot par : parkinglots) {
			Rparkinglot rp = new Rparkinglot();
		rp.setDescription(par.getDescription());
		rp.setId(par.getId());
		rp.setImage(par.getImage());
		rp.setInit_price(par.getInit_price());
		rp.setLatitude(par.getLatitude());
		rp.setLongitude(par.getLongitude());
		rp.setName(par.getName());
		rp.setPark_number(par.getPark_number());
		rp.setPer_hour_price(par.getPer_hour_price());
		rp.setSurplus_park_num(par.getSurplus_park_num());
		rp.setStatus(par.getStatus());
		rps.add(rp);
		}
		if(!rps.isEmpty()) {
			return rps;
		}else {
			return null;
		}	
	}

	@Override
	public List<Rparkinglot> deleteParklots(HttpSession httpsession,int parklotId) {
		// TODO Auto-generated method stub
		User user = (User) httpsession.getAttribute("user");
		User usertransit = new User();
		usertransit.setId(user.getId());
		addParklotDao.deleteParkLots(parklotId);
		
		List<ParkingLot> parkinglots = addParklotDao.parkList(usertransit.getId());
		List<Rparkinglot> rparks = new ArrayList<Rparkinglot>();
		for(ParkingLot par : parkinglots) {
			Rparkinglot rp = new Rparkinglot();
		rp.setDescription(par.getDescription());
		rp.setId(par.getId());
		rp.setImage(par.getImage());
		rp.setInit_price(par.getInit_price());
		rp.setLatitude(par.getLatitude());
		rp.setLongitude(par.getLongitude());
		rp.setName(par.getName());
		rp.setPark_number(par.getPark_number());
		rp.setPer_hour_price(par.getPer_hour_price());
		rp.setSurplus_park_num(par.getSurplus_park_num());
		rp.setStatus(par.getStatus());
		rparks.add(rp);
		}
		if(!rparks.isEmpty()) {
			return rparks;
		}else {
			return null;
		}			
	}

	@Override
	public List<Rparkinglot> updateParklots(HttpSession httpsession,ParkingLot parkinglot) {
		// TODO Auto-generated method stub
		User user = (User) httpsession.getAttribute("user");
		addParklotDao.updateParkLots(parkinglot,user);
		userDao.update(user);
		List<ParkingLot> parkinglots = user.getParking_lots();
		List<Rparkinglot> rparks = new ArrayList<Rparkinglot>();
		for(ParkingLot par : parkinglots) {
			Rparkinglot rp = new Rparkinglot();
		rp.setDescription(par.getDescription());
		rp.setId(par.getId());
		rp.setImage(par.getImage());
		rp.setInit_price(par.getInit_price());
		rp.setLatitude(par.getLatitude());
		rp.setLongitude(par.getLongitude());
		rp.setName(par.getName());
		rp.setPark_number(par.getPark_number());
		rp.setPer_hour_price(par.getPer_hour_price());
		rp.setSurplus_park_num(par.getSurplus_park_num());
		rp.setStatus(par.getStatus());
		
		rparks.add(rp);
		}
		
		if(!rparks.isEmpty()) {
			return rparks;
		}else {
			return null;
		}			
	}

}
