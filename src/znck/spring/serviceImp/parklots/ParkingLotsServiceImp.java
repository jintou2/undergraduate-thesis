package znck.spring.serviceImp.parklots;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import znck.bean.constant.ParkStatus;
import znck.bean.dto.Location;
import znck.bean.dto.Rcartype;
import znck.bean.dto.Rparkinglot;
import znck.bean.dto.RpklCartype;
import znck.hb.entity.CarType;
import znck.hb.entity.Park;
import znck.hb.entity.ParkingLot;
import znck.hb.entity.User;
import znck.spring.dao.ParkDao;
import znck.spring.dao.ParkingLotDao;
import znck.spring.service.parklots.ParkingLotService;

@Service
@Transactional
public class ParkingLotsServiceImp implements ParkingLotService {

	@Autowired
	ParkingLotDao parkingLotDao;
	@Autowired
	ParkDao parkDao;
	
	@Override
	public List<ParkingLot> queryparklots() {
		// TODO Auto-generated method stub
		return parkingLotDao.selectworkablePL();
	}

	@Override
	public int parkOder(int parklotID ,Park park, HttpSession httpsession) {
		// TODO Auto-generated method stub
		User user = (User) httpsession.getAttribute("user");
		List<Park> parks = parkDao.selectUnfinishedPark(park.getCar_number());
		ParkingLot parklot = parkingLotDao.get(parklotID);
		if(parks != null) {
			return ParkStatus.oder_repeat;
		}else if(parklot.getSurplus_park_num() < 1) {
			return ParkStatus.failure;
		}
		else {  //可以预定
			park.setUser(user);
			park.setParking_lot(parklot);
			park.setStatus(ParkStatus.odered);
			parkDao.save(park);
			parkingLotDao.surplusParkSub(parklotID);
		    return ParkStatus.odered;
		}
	}

	@Override
	public List<Rparkinglot> ParklotsSearch(Location location, int num) {
		// TODO Auto-generated method stub
		List<ParkingLot> parklots = parkingLotDao.selectworkablePL();
		for(int j = 1; j<parklots.size(); j++) {
			for(int i = 0; i<parklots.size()-1;i++) {
				double dis1 = Math.pow(parklots.get(i).getLatitude() - location.getLatitude(),2)
						+ Math.pow(parklots.get(i).getLongitude()-location.getLongitude(),2);
				double dis2 = Math.pow(parklots.get(i+1).getLatitude() - location.getLatitude(),2)
						+ Math.pow(parklots.get(i+1).getLongitude()-location.getLongitude(),2);
				
				if(dis1 > dis2) {
					ParkingLot parklot = parklots.get(i);
					parklots.set(i, parklots.get(i+1));
					parklots.set(i, parklot);
				}
			}
		}

		List<Rparkinglot> rps = new ArrayList<Rparkinglot>();
		for(ParkingLot par : parklots) {
			Rparkinglot rp = new Rparkinglot();
		rp.setDescription(par.getDescription());
		rp.setId(par.getId());
		rp.setImage(par.getImage());
		rp.setInit_price(par.getInit_price());
		rp.setLatitude(par.getLatitude());
		rp.setLongitude(par.getLongitude());
		rp.setName(par.getName());
		rp.setPark_number(par.getPark_number());
		rp.setPer_hour_price(par.getPer_hour_price());
		rp.setSurplus_park_num(par.getSurplus_park_num());
		rp.setStatus(par.getStatus());
		rp.setAddress(par.getAddress());
		System.out.println(par.getAddress());
		rp.setAreaId(par.getAreaId());
		
		rps.add(rp);
		}
		
		if(parklots.size() <= num) {
			return rps;
		}else {
			return rps.subList(0, num - 1);
		}
		
	}

	@Override
	public RpklCartype parklotprofile(int parklotId) {
		// TODO Auto-generated method stub
		ParkingLot par = parkingLotDao.get(parklotId);
		RpklCartype rp = new RpklCartype();
	rp.setDescription(par.getDescription());
	rp.setId(par.getId());
	rp.setImage(par.getImage());
	rp.setInit_price(par.getInit_price());
	rp.setLatitude(par.getLatitude());
	rp.setLongitude(par.getLongitude());
	rp.setName(par.getName());
	rp.setPark_number(par.getPark_number());
	rp.setPer_hour_price(par.getPer_hour_price());
	rp.setSurplus_park_num(par.getSurplus_park_num());
	rp.setStatus(par.getStatus());
	rp.setAddress(par.getAddress());
	rp.setAreaId(par.getAreaId());
	
	for(CarType ct : par.getCartypes()) {
		Rcartype rc = new Rcartype();
		rc.setId(ct.getId());
		rc.setType(ct.getType());
		
		rp.getCartypes().add(rc);
	}
		
		return rp;
	}

	@Override
	public boolean parkCancel(int parkId, HttpSession httpsession) {
		// TODO Auto-generated method stub
		return parkingLotDao.parkcancel(parkId, (User) httpsession.getAttribute("user"));
	}

	@Override
	public boolean InStorage(int parklotid, String carnumber) {
		// TODO Auto-generated method stub
		return parkingLotDao.InStorageSt(parklotid, carnumber);
	}

	@Override
	public boolean OutStorage(int parklotid, String carnumber) {
		// TODO Auto-generated method stub
		return parkingLotDao.OutStorageSt(parklotid, carnumber);
	}

}
