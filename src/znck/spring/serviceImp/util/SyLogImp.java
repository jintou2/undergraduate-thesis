package znck.spring.serviceImp.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import znck.spring.service.util.SyLog;

@Service
public class SyLogImp implements SyLog {

	@Override
	public void warn(Class<?> T, String news) {
		// TODO Auto-generated method stub
		Logger logger = LogManager.getLogger(T.getName());
		logger.warn(news);
	}

	@Override
	public void error(Class<?> T, String news) {
		// TODO Auto-generated method stub
		Logger logger = LogManager.getLogger(T.getName());
		logger.error(news);
	}

	@Override
	public void fatal(Class<?> T, String news) {
		// TODO Auto-generated method stub
		Logger logger = LogManager.getLogger(T.getName());
		logger.fatal(news);
	}

	@Override
	public void debug(Class<?> T, String news) {
		// TODO Auto-generated method stub
		Logger logger = LogManager.getLogger(T.getName());
		logger.debug(news);
	}

	@Override
	public void trace(Class<?> T, String news) {
		// TODO Auto-generated method stub
		Logger logger = LogManager.getLogger(T.getName());
		logger.trace(news);
	}

}
