package znck.spring.serviceImp.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import znck.spring.service.util.URLService;

@Service
public class URLServiceImp implements URLService {

	@Override
	public String getUrlParameter(String url, String key) {
		// TODO Auto-generated method stub
		Map<String, String> paramMap = new HashMap<>();
		String str = url.substring(url.indexOf("?") + 1);  //获取"?"之后的字符串
		String[] parameters = str.split("&");
		for(String param : parameters) {
			String values[] = param.split("=");
            paramMap.put(values[0], values[1]);
		}
		return paramMap.get(key);
	}

}
