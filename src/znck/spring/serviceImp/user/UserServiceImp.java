package znck.spring.serviceImp.user;

import java.util.List;


import javax.servlet.http.HttpSession;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.sf.json.JSONObject;
import znck.hb.entity.Park;
import znck.hb.entity.User;
import znck.spring.dao.ParkDao;
import znck.spring.dao.UserDao;
import znck.spring.service.user.UserService;
import znck.spring.serviceImp.util.SystemServiceImp;

@Service
@Transactional
public class UserServiceImp implements UserService {


	@Autowired
	UserDao userDao;
	@Autowired
	ParkDao parkDao;
	@Autowired
	SystemServiceImp systemServiceImp;
	
	@Override
	public boolean phoneRegister(User user, String verification_code, HttpSession httpsession) {
		// TODO Auto-generated method stub
		//获取验证码
		String v_code = String.valueOf(httpsession.getAttribute(user.getPhone()));
		if(user.getPassword()!=null&&!user.getPassword().equals("")) {
		if(v_code.equals(verification_code)) {
			User ur = userDao.PhoneUserAdd(user);
			//System.out.print(ur);
			if(ur!=null) {
				httpsession.setAttribute("user", ur);
				return true;
			}
		}
		}
		return false;
	}

	@Override
	public boolean phoneLogin(User user, HttpSession httpsession) {
		// TODO Auto-generated method stub
		User ur = userDao.PhoneUserLogin(user);
		if(ur!=null) {
			httpsession.setAttribute("user", ur);
			return true;
		}
		return false;
	}

	@Override
	public List<Park> selectParks(HttpSession httpsession) {
		// TODO Auto-generated method stub
		User user = (User) httpsession.getAttribute("user");
		List<Park> parks = parkDao.queryparks(user);
		for(Park park : parks) { //遍历查询车场
			Hibernate.initialize(park.getParking_lot()); //强制立即加载相关属性
		}
		return parks;
	}

	@Override
	public User weiXinRegister(String openId, String access_token) {
		// TODO Auto-generated method stub
		User user = new User();
		user.setOpenID(openId);
		
       // System.out.println("openid:    "+openId);
        //System.out.println("access_token:    "+access_token);
		
		if(userDao.weiXinUserSelect(openId)) {
			return userDao.WeiXinUserAdd(user);
		}else {
			String get_user_info = systemServiceImp.getProperty_key("get_user_info");
			String accessToken = systemServiceImp.getProperty_key("accessToken");
			String openid = systemServiceImp.getProperty_key("openid");
			get_user_info = get_user_info.replaceAll(accessToken, access_token);
			get_user_info = get_user_info.replaceAll(openid, openId);
			
			StringBuffer buffer = systemServiceImp.HttpsSend(get_user_info, "GET", null);
			JSONObject jsonObj = JSONObject.fromObject(buffer.toString());
			
			user.setName(jsonObj.getString("nickname"));
			
			return userDao.WeiXinUserAdd(user);
		}
	}

	@Override
	public boolean weiXinIsRegister(String openId) {
		// TODO Auto-generated method stub
		if(userDao.weiXinUserSelect(openId)) {
			return false;
		}else {
			return true;
		}
	}

	/**
	 * 此处注意，如果将httpsession中保存的实体化类，再一次由脱管状态变为持久化状态，
	 * 视相关操作而定有可能改变对象中延迟加载对象的相关状态，比如该函数，会导致除第一次查询以外的其他查询不再去数据库读取数据，
	 * 强调此处和hibernate一级缓存没有关系
	 */
    /**
	@Override
	public List<Park> selectParks(HttpSession httpsession) {
		// TODO Auto-generated method stub
		User user = (User) httpsession.getAttribute("user");
		User ur = new User();
		ur.setId(user.getId());
		userDao.updateObj(ur);
		System.out.println("停车记录数量："+"    "+ur.getParks().size());
		return ur.getParks();
	}
	  */
	
	
}
