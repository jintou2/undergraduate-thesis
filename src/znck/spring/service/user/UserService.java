package znck.spring.service.user;

import java.util.List;

import javax.servlet.http.HttpSession;

import znck.hb.entity.Park;
import znck.hb.entity.User;

public interface UserService {

	/**
	 * 手机用户注册
	 * @param user 包含phone和password的user对象
	 * @param verification_code 验证码
	 * @return 成功返回true，否则false
	 */
	boolean phoneRegister(User user,String verification_code, HttpSession httpsession);
	
	/**
	 * 手机用户登录
	 * @param user 包含phone和password的user对象
	 * @return 成功返回true，否则false
	 */
	boolean phoneLogin(User user, HttpSession httpsession);
	
	/**
	 * @return 我的停车记录
	 */
	List<Park> selectParks(HttpSession httpsession);
	
	/**
	 * 微信用户注册
	 * @param openId 微信openid
	 * @param access_token 用于获取用户信息
	 * @return 返回微信用户，没有则新注册，有则读取已有用户数据
	 */
	User weiXinRegister(String openId, String access_token);
	
	/**
	 * @author wxj233
	 * @param openId 微信openId
	 * @return 微信用户是否已注册,是-true、否-false
	 */
	boolean weiXinIsRegister(String openId);
	
}
