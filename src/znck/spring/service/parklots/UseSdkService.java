package znck.spring.service.parklots;

import znck.bean.weixin.jssdk.JsSDK;

public interface UseSdkService {
	
	/**
	 * 
	 * 获取ticket
	 */
	public String getTiket();
	
	/**
	 * 将参数按照字段名的 ASCII 码从小到大排序（字典序）后，使用 URL 键值
	 * 对的格式（即key1=value1&key2=value2…）拼接成字符串
	 */
	public JsSDK formatQueryParaMap();
}
