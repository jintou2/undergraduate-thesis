package znck.spring.service.parklots;


import java.util.List;

import javax.servlet.http.HttpSession;

import znck.bean.dto.Rparkinglot;
import znck.hb.entity.ParkingLot;

public interface AddParklotService {
	
	/**
	 * 添加停车场
	 */
	public boolean parkingLotAdd(HttpSession httpsession,ParkingLot parkinglot,String cartype);
	
	/**
	 * 查询全部可用停车场
	 */
	public List<Rparkinglot> queryParkinglots();
	
	/**
	 * 用户查询停车场
	 */
	public List<Rparkinglot> usersParkinglots(HttpSession httpsession);
	
	/**
	 * 删除停车场
	 */
	public List<Rparkinglot> deleteParklots(HttpSession httpsession,int parklotId);
	
	/**
	 * 更新停车场
	 */
	public List<Rparkinglot> updateParklots(HttpSession httpsession,ParkingLot parkinglot);
	
	
}
