package znck.spring.service.parklots;

import java.util.List;

import javax.servlet.http.HttpSession;

import znck.bean.dto.Location;
import znck.bean.dto.Rparkinglot;
import znck.bean.dto.RpklCartype;
import znck.hb.entity.Park;
import znck.hb.entity.ParkingLot;

public interface ParkingLotService {

	/**
	 * 查询车场
	 * @return 返回运行正常的车场
	 */
	List<ParkingLot> queryparklots ();
	
	/**
	 * 车位订购
	 * @param id 车场id
	 * @param car_number 车辆牌照
	 * @return 订单状态
	 */
	int parkOder(int parklotID, Park park, HttpSession httpsession);
	
	/**
	 * 订单取消
	 * @param parkId 订单编号
	 * @return true/false
	 */
	boolean parkCancel(int parkId, HttpSession httpsession);
	
	/**
	 * 车场搜索
	 * @param num 返回的最近搜索数量
	 * @param location 搜索目标位子
	 * @return 附近搜索的车库
	 */
	List<Rparkinglot> ParklotsSearch(Location location, int num);
	
	/**
	 * 查询当前车场详细信息
	 * @param parklotId 车场id
	 * @return 车场详细信息
	 */
	RpklCartype parklotprofile(int parklotId);
	
	/**
	 * 车辆入库
	 * @param parklotid 车库id
	 * @param carnumber 车牌号
	 */
	boolean InStorage(int parklotid, String carnumber);
	
	/**
	 * 车辆出库
	 * @param parklotid 车库id
	 * @param carnumber 车牌号
	 */
	boolean OutStorage(int parklotid, String carnumber);
	
}
