package znck.spring.service.weixin;

import org.dom4j.Document;

public interface XmlResponseService {

	/**
	 * 微信消息响应
	 * @param document 接收到的xml文档
	 * @return 响应内容，xml或者""
	 */
	String XmlResponse(Document document);
	
	/**
	 * 关注事件响应
	 * @param document 接收到的xml文档
	 * @return 响应内容，xml或者""
	 */
	String Subscribe(Document document);
	
	/**
	 * 默认响应
	 * @param document 接收到的xml文档
	 * @return 响应内容，xml或者""
	 */
	String defaultResponse(Document document);
	
}
