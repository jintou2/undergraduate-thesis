package znck.spring.service.weixin;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.dom4j.Document;

public interface XmlService {

	/**
	 * 获取一个微信Xml对象，支持中文字符
	 * @param in 字符输入流
	 * @return Dom4j的Document类型 
	 */
	Document ObtainXmlDoc(InputStreamReader in);
	
	/**
	 * 获取一个微信Xml对象
	 * @param in 字节输入流
	 * @return Dom4j的Document类型
	 */
	Document ObtainXmlDoc(InputStream in);
	
	/**
	 * 将一个微信Xml输入流转换未Javabean对象，javabean必须与xml匹配，使用的是XStream
	 * <T> 声明该方法返回为泛型
	 * @param in 字符输入流
	 * @param <T>
	 * @return 返回type所定义的类型
	 */
	<T> T getBean(Class<T> type, InputStreamReader in);
	
	/**
	 * 将一个微信Xml输入流转换未Javabean对象，javabean必须与xml匹配，使用的是XStream
	 * <T> 声明该方法返回为泛型
	 * @param in 字节输入流
	 * @param <T>
	 * @return 返回type所定义的类型
	 */
	<T> T getBean(Class<T> type, InputStream in);
	
	/**
	 * 将一个Document对象转换未Javabean
	 * <T> 声明该方法返回为泛型
	 * @param doc Document对象
	 * @param <T>
	 * @return 返回type所定义的类型
	 */
	<T> T getBean(Class<T> type, Document doc);
	
	/**
	 * 将Javabean对象转换为微信Xml字符串
	 * @param obj javabean对象
	 * @return Document对象
	 */
	String JavabeanToXml(Object obj);
	
	/**
	 * 将Javabean对象转换为Dom4j的Document对象
	 * @param obj javabean对象
	 * @return Document对象
	 */
	Document JavabeanToDocument(Object obj);
}
