package znck.spring.service.weixin;

public interface ValidationService {

	/**
	 * 将字节数组转换为十六进制字符串
	 * @param bytearray 字节数组
	 */
	String byteToStr(byte[] byteArray);
	
	/**
	 * 将字节转换为十六进制字符串
	 * @mByte 字节
	 */
    String byteToHexStr(byte mByte);
    
    /**
     * @param signature 微信加密签名
	 * @param timestamp 时间戳
	 * @param nonce 随机数
	 * @token 微信中设置的token
	 * 
	 * @return 验证成功返回true，否则返回false
     */
    boolean checkSignature(String signature, String timestamp, String nonce, String token);
}
