package znck.spring.service.weixin;


public interface AccessTokenService {

	
	/**
	 * 获取微信AccessToken,并在servletContext中存入
	 * @return 微信access_token
	 */
	String getAccessToken();
}
