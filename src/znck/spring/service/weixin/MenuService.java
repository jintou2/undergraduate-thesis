package znck.spring.service.weixin;

import java.util.List;


import net.sf.json.JSONObject;
import znck.bean.weixin.menu.SynthesisMenu;

public interface MenuService {
	
	/**
	 * 菜单修改,添加
	 * @param json字符串menu
	 * @deprecated  新方法menuEdit()
	 */
	boolean menuModify(String jsonmenu);
	
	/**
	 * 菜单修改,添加
	 * 修改后会在url中嵌入微信回调url，以便于用于微信登录
	 * @param json字符串menu
	 */
	boolean menuEdit(String jsonmenu);
	
	/**
	 * 获取回调URL
	 * @param SynthesisMenus  传入菜单类，将菜单类中url替换，只取回调url
	 */
	List<SynthesisMenu> getCallBackUrl(List<SynthesisMenu> SynthesisMenus);
	
	/**
	 * 菜单查询
	 * @return jsonObj
	 */
	JSONObject menuQuery();
	
	/**
	 * 删除菜单,默认删除所有菜单
	 * @return true/false
	 */
	boolean menuDelete();
}
