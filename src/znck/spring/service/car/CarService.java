package znck.spring.service.car;

import javax.servlet.http.HttpSession;

import znck.hb.entity.Car;

public interface CarService {

	/**
	 * 添加车辆
	 * @param cartypeId 车辆类型ID
	 * @param car 里面包括车辆牌照
	 * @return 添加成功返回true,失败返回false
	 */
	boolean CarAdd(HttpSession httpsession, int cartypeId, Car car);
}
