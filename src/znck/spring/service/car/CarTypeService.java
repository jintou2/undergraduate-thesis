package znck.spring.service.car;

import java.util.List;

import znck.bean.dto.Rcartype;

public interface CarTypeService {

	/**
	 * 查询系统所有的车辆类型
	 * @return 所有车辆类型
	 */
	List<Rcartype> getAllCarType();
}
