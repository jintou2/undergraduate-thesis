package znck.spring.service.util;

import javax.servlet.http.HttpSession;

public interface SMSService {

	/**
	 * 验证码发送,并且将验证码在httpsession中保存60秒
	 * @param phone 电话号码
	 * @return 发送的验证码/失败返回null
	 * @deprecated 阿里云接口修改，导致无法使用,新方法SMS_Send(String phone, HttpSession httpsession)
	 */
	String Verification_code_send(String phone, HttpSession httpsession);
	
	/**
	 * @since 2018/11/18
	 * @param phone 电话号码
	 * @return 发送的验证码/失败返回null
	 * @author wxj233
	 */
	boolean SMS_Send(String phone, HttpSession httpsession);
	
}
