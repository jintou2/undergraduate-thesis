package znck.spring.service.util;

public interface URLService {

	/**
	 * 解析url字符串,获取键-值
	 * @author wxj233
	 * @param url
	 * @param key
	 */
	String getUrlParameter(String url, String key);
}
