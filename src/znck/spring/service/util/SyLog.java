package znck.spring.service.util;

/**
 * @author wxj233
 *
 */
public interface SyLog {

	/**
	 * 警告信息
	 */
	void warn(Class<?> T, String news);
	
	/**
	 * 一般错误
	 */
	void error(Class<?> T, String news);
	
	/**
	 * 致命错误
	 */
	void fatal(Class<?> T, String news);
	
	/**
	 * 调试信息
	 */
	void debug(Class<?> T, String news);
	
	/**
	 * 跟踪信息
	 */
	void trace(Class<?> T, String news);
}
