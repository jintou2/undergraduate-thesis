package znck.spring.service.util;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public interface SystemService {

	/**
	 * 获取SystemSet.properties的值
	 */
	String getProperty_key(String key);
	
	/**
	 * 发送https请求
	 * StringBuffer与String的区别在于String是不可变对象而StringBuffer是1可变对象，String可以通过“=”赋值，而StringBuffer不可以
	 * 并且由于String 对象是不可变对象,每次操作Sting 都会重新建立新的对象来保存新的值.这样原来的对象就没用了,就要被垃圾回收.这也是要影响性能的. 
	 * 
	 * @param url 访问的url
	 * @param type 请求方式GET或者POST
	 * @data post方式要传输的数据
	 */
	StringBuffer HttpsSend(String url, String type, String data);
	
	/**
	 * javabean转json字符串
	 * @param obj Javabean对象
	 * @return json字符串
	 */
	String JavabeanTojson(Object obj);
	
	/**
	 * javabean转JSONObject对象
	 * @param obj Javabean对象
	 * @return json字符串
	 */
	JSONObject JavabeanToJSONObject(Object obj);
	
	/**
	 * javaList对象转jsonarray字符串
	 * @param obj List对象
	 * @return jsonarray字符串
	 */
	String JavabeanTojsonarray(Object obj);
	
	/**
	 * 将json字符串转Javabean对象
	 * @param type 要转换的Javabean类型
	 * @param json json字符串
	 * @return <T>所指定的javabean对象
	 */
	<T> T jsonTojavabean(Class<T> type, String json);
	
	/**
	 * 将json字符串转Javabean对象
	 * @param type 要转换的Javabean类型
	 * @param json json字符串
	 * @return <T>所指定的javabean对象
	 */
	<T> T jsonTojavabean(Class<T> type, String json, Map<String, Class<T>> classmap);

	/**
	 * 将JSONArray转换为List<?>对象
	 */
	<T> List<T> JSONArrayToListbean(Class<T> type, JSONArray jsonarray, Map<String, Class<T>> classmap);
	
	/**
	 * 将JSONArray字符串转换为List<?>对象
	 */
	<T> List<T> JSONArrayToListbean(Class<T> type, String jsonarray, Map<String, Class<T>> classmap);
	
	/**
	 * 将JSONArray转换为List<?>对象
	 */
	<T> List<T> JSONArrayToListbean(Class<T> type, JSONArray jsonarray);
	
	/**
	 * 将JSONArray字符串转换为List<?>对象
	 */
	<T> List<T> JSONArrayToListbean(Class<T> type, String jsonarray);
}
