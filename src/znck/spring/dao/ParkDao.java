package znck.spring.dao;

import java.util.List;



import org.springframework.stereotype.Repository;

import znck.bean.constant.ParkStatus;
import znck.hb.entity.Park;
import znck.hb.entity.User;

@Repository
public class ParkDao extends BaseDao<Park> {

	/**
	 * 查询用户未完成订单
	 * @param userID 用户id
	 * @return 当前牌照下车辆未完成订单
	 */
	@SuppressWarnings("unchecked")
	public List<Park> selectUnfinishedPark(String car_number){
		List<?> park = getlocalSession().createQuery("select park from Park park where park.car_number = :car_number "
				+ "and park.status != " + ParkStatus.oder_finish 
				+ "and park.status != " + ParkStatus.oder_cancel)
				.setParameter("car_number", car_number)
				.getResultList();
		
		if(!park.isEmpty()) {
			return (List<Park>) park;
		}
		return null;
	}
	
	/**
	 * 查询用户订单
	 * @param user 用户
	 */
	@SuppressWarnings("unchecked")
	public List<Park> queryparks(User user) {
		List<Park> parks = getlocalSession().createQuery("select park from Park park where park.user = :user")
				.setParameter("user", user)
				.getResultList();
		if(!parks.isEmpty()) {
			return (List<Park>) parks;
		}
		return null;
	}
	
}
