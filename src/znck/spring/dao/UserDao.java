package znck.spring.dao;

import java.util.List;


import org.springframework.stereotype.Repository;

import znck.hb.entity.User;

@Repository
public class UserDao extends BaseDao<User> {

	/**
	 * 微信用户添加
	 * @param user 要注册的微信用户
	 * @return 数据库中的用户，已经注册了的
	 */
	public User WeiXinUserAdd(User user) {
		List<?> users  = getlocalSession().createQuery("select user from User user where user.openID = :openID")
				.setParameter("openID", user.getOpenID())
				.getResultList();
		if(users.isEmpty()){
			save(user);
			return user;
		}else{
			return (User) users.get(0);
		}
	};
	
	/**
	 * @author wxj233
	 * @since 2018/10/30
	 * @return 查询微信用户，有-true、无-false
	 */
	public boolean weiXinUserSelect(String openid) {
		List<?> users  = getlocalSession().createQuery("select user from User user where user.openID = :openID")
				.setParameter("openID", openid)
				.getResultList();
		if(users.isEmpty()){
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * 手机用户添加
	 * @param user 包含phone和password的user对象
	 */
	public User PhoneUserAdd(User user) {
		List<?> users = getlocalSession().createQuery("select user from User user where user.phone = :rphone")
		.setParameter("rphone", user.getPhone())
		.getResultList();
		
		if(users.isEmpty()){
			save(user);
			return user;
		}else{
			return null;
		}
	}
	
	/**
	 * 用户查询
	 * @param 包含phone和password的user对象
	 */
	public User PhoneUserLogin(User user) {
		List<?> users = getlocalSession().createQuery("select user from User user where user.phone = :phone and password = :password")
				.setParameter("phone", user.getPhone())
				.setParameter("password", user.getPassword())
				.getResultList();
		
		if(!users.isEmpty()) {
			//System.out.println("停车记录数量："+"    "+((User) users.get(0)).getParks().size());
			return (User) users.get(0);
		}else {
			return null;
		}
	}
	
}
