package znck.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import znck.bean.constant.ParkingLotStatus;
import znck.hb.entity.ParkingLot;
import znck.hb.entity.User;

@Repository
public class AddParklotDao extends BaseDao<ParkingLot> {

	/**
	 * 查询运行状态正常的车库
	 */
	@SuppressWarnings("unchecked")
	public List<ParkingLot> queryParkingLots(){
		return getlocalSession().createQuery("select parkingLot from ParkingLot parkingLot where parkingLot.status = 1")
				.getResultList();
	}
	
	/**
	 * 删除停车场
	 */
	public void deleteParkLots(int parkinglotId) {
		ParkingLot parkinglot = get(parkinglotId);
		parkinglot.setStatus(0);
		update(parkinglot);
	}
	
	/**
	 * 更新停车场
	 */
	public void updateParkLots(ParkingLot parkingLot,User user) {
		int id = parkingLot.getId();
		ParkingLot parkinglots = get(id);
		parkinglots.setAddress(parkingLot.getAddress());
		parkinglots.setDescription(parkingLot.getDescription());
		parkinglots.setName(parkingLot.getName());
		parkinglots.setLatitude(parkingLot.getLatitude());
		parkinglots.setLongitude(parkingLot.getLongitude());
		parkinglots.setUser(user);
		parkinglots.setPark_number(parkingLot.getPark_number());
		parkinglots.setInit_price(parkingLot.getInit_price());
		parkinglots.setStatus(parkingLot.getStatus());
		parkinglots.setInit_price(parkingLot.getInit_price());
		parkinglots.setPark_number(parkingLot.getPark_number());
		update(parkinglots);
		
	}
	/**
	 * 查询用户的停车场
	 */
	@SuppressWarnings("unchecked")
	public List<ParkingLot> parkList(int userId) {
		List<ParkingLot> parklots=   getlocalSession().createQuery("select parkingLot from ParkingLot parkingLot where user_id = :userId "
				+ "and parkingLot.status = " + ParkingLotStatus.running)
				.setParameter("userId", userId)
				.getResultList();
		return parklots;
	}
	
}
