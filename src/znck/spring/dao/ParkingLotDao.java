package znck.spring.dao;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import znck.bean.constant.ParkStatus;
import znck.bean.constant.Price;
import znck.hb.entity.Park;
import znck.hb.entity.ParkingLot;
import znck.hb.entity.User;

@Repository
public class ParkingLotDao extends BaseDao<ParkingLot> {

	@Autowired
	ParkDao parkDao;
	
	/**
	 * 查询运行状态正常的车库
	 */
	public List<ParkingLot> selectParkingLots(){
		return query("select parkingLot from ParkingLot parkingLot where parkingLot.status = 1");
	}
	
	/**
	 * 查询运行正常且有剩余车位的车库
	 */
	public List<ParkingLot> selectworkablePL(){
		return query("select parkingLot from ParkingLot parkingLot where parkingLot.status = 1 "
				+ "and parkingLot.surplus_park_num > 0");
	}
	
	/**
	 * 剩余车位减一
	 * @param ParkingLotID 车场id
	 * @return 剩余车位
	 */
	public int surplusParkSub(int ParkingLotID) {
		ParkingLot parkingLot = get(ParkingLotID);
		parkingLot.setSurplus_park_num(parkingLot.getSurplus_park_num() - 1);
		update(parkingLot);
		return parkingLot.getSurplus_park_num();
	}
	
	/**
	 * 剩余车位加一
	 * @param ParkingLotID 车场id
	 * @return 剩余车位
	 */
	public int surplusParkAdd(int ParkingLotID) {
		ParkingLot parkingLot = get(ParkingLotID);
		parkingLot.setSurplus_park_num(parkingLot.getSurplus_park_num() + 1);
		update(parkingLot);
		return parkingLot.getSurplus_park_num();
	}
	
	/**
	 * 取消未完成订单/更新订单状态
	 * @param parkId 订单编号 
	 * @return true/false
	 */
	public boolean parkcancel(int parkId,User user) {
		long systime = System.currentTimeMillis();  //取消订单时间
		Park park = parkDao.get(parkId);
		double price = priceCalculate(park, systime);
		int tf = getlocalSession().createQuery("update from Park park set park.status = :cancel, park.price = :price, park.enddate = :enddate"
				+ " where park.user = :user and park.id = :parkId "
				+ "and park.status = :odered")
		.setParameter("cancel", ParkStatus.oder_cancel)
		.setParameter("price", price)
		.setParameter("enddate", new Date(systime))
		.setParameter("user", user)
		.setParameter("parkId", parkId)
		.setParameter("odered", ParkStatus.odered)
		.executeUpdate();
		
		if(tf == 0) {
			return false;
		}else {
			return true;
		}
		
	}
	
	/**
	 * 价格计算
	 * @param systime 订单结束时间
	 */
	public double priceCalculate(Park park, long systime) {
		if(park.getStartdate() == null) {
			return 0;
		}else {
			int submin = minutesub(systime, park.getStartdate().getTime());
			if(submin <= 30) {
				return Price.priceIn30;
			}else { //保留两位小数，向上取整
				return Math.round(Price.priceOut30 * Math.ceil((submin/30)) * 100)/100;
			}
		}
		
	}
	
	/**
	 * 计算时间差，小数部分去掉不要，只取整
	 * @param dateA,dateB两个时间毫秒数
	 * @return 两个时间差的绝对值
	 */
	public int minutesub(long dateA, long dateB) {
		return (int) Math.abs(((dateA-dateB)/1000/60));
	}
	
	/**
	 * 车辆入库状态改变
	 * @param parklotod 车场id
	 * @param carnumber 车牌号
	 */
	@SuppressWarnings("unchecked")
	public boolean InStorageSt (int parklotid, String carnumber) {
		long systime = System.currentTimeMillis();   //入库时间
		ParkingLot parkingLot = new ParkingLot();
		parkingLot.setId(parklotid);
		List<Park> parks = getlocalSession().createQuery("select park from Park park where park.car_number = :carnumber"
				+ "and park.parking_lot = parkingLot"
				+ "and park.status = :ordered")
		.setParameter("carnumber", carnumber)
		.setParameter("ordered", ParkStatus.odered)
		.getResultList();
		
		Park park = null;
		if(parks.size()>0) {
			park = parks.get(0);
			long Temtime = park.getStartdate().getTime();
			if(systime < Temtime) {
				Temtime = systime;
			}
			park.setStartdate(new Date(Temtime));
			park.setStatus(ParkStatus.oder_underway);
			
			parkDao.update(park);
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 车辆出库状态改变
	 * @param parklotod 车场id
	 * @param carnumber 车牌号
	 */
	@SuppressWarnings("unchecked")
	public boolean OutStorageSt(int parklotid, String carnumber) {
		long systime = System.currentTimeMillis();   //出库时间
		ParkingLot parkingLot = new ParkingLot();
		parkingLot.setId(parklotid);
		List<Park> parks = getlocalSession().createQuery("select park from Park park where park.car_number = :carnumber"
				+ "and park.parking_lot = parkingLot"
				+ "and park.status = :oder_underway")
		.setParameter("carnumber", carnumber)
		.setParameter("oder_underway", ParkStatus.oder_underway)
		.getResultList();
		
		if(parks.size()>0) {
			Park park = parks.get(0);
			park.setStatus(ParkStatus.oder_finish);
			park.setPrice(priceCalculate(park, systime));
			
			parkDao.update(park);
			return true;
		}else {
			return false;
		}
		
		
	}
	
}
