package znck.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import znck.hb.entity.CarType;

@Repository
public class CarTypeDao extends BaseDao<CarType>{

	/**
	 * 查询所有的车辆类型
	 * @return 返回系统所有车辆类型
	 */
	public List<CarType> queryAllType(){
		return query("select cartype from CarType cartype");
	}
}
