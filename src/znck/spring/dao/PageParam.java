package znck.spring.dao;

/**
 * 分页参数
 * @author wxj233
 * @version 1.0
 */
public class PageParam {

	/**
	 * 默认页码
	 */
	private static final int DEFAULT_PAGE_NUMBER = 1;
	
	/**
	 * 默认每页记录数
	 */
	private static final int DEFAULT_PAGE_SIZE = 10;
	
	/**
	 * 每页最大记录数
	 */
	private static final int MAX_PAGE_SIZE = 1000;
	
	/**
	 * 页码
	 */
	private int pageNumber = DEFAULT_PAGE_NUMBER;
	
	/**
	 * 每页记录数
	 */
	private int pageSize = DEFAULT_PAGE_SIZE;

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public static int getDefaultPageNumber() {
		return DEFAULT_PAGE_NUMBER;
	}

	public static int getDefaultPageSize() {
		return DEFAULT_PAGE_SIZE;
	}

	public static int getMaxPageSize() {
		return MAX_PAGE_SIZE;
	}
	
	
}
