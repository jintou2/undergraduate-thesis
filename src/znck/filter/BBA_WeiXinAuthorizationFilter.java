package znck.filter;

import java.io.IOException;






import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;
import znck.hb.entity.User;
import znck.spring.service.user.UserService;
import znck.spring.service.util.SystemService;
import znck.spring.serviceImp.util.SpringContextHolder;

/**
 * Servlet Filter implementation class WeiXinAuthorizationFilter
 * 与UserFilter过滤器过滤类容一致
 */
@WebFilter({"/car/*","/user/myorder.znck","/weixin/menu/viewMenu.znck"})
//@Component
public class BBA_WeiXinAuthorizationFilter implements Filter {

	
	//WebApplicationContext wac;
	
    /**
     * Default constructor. 
     */
    public BBA_WeiXinAuthorizationFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		//userServiceImp = wac.getBean(UserServiceImp.class);
		//systemServiceImp = wac.getBean(SystemServiceImp.class);
		UserService userServiceImp = (UserService) SpringContextHolder.getBean("userServiceImp");
		SystemService systemServiceImp = (SystemService) SpringContextHolder.getBean("systemServiceImp");
		
		HttpServletRequest req = (HttpServletRequest)request;
		//System.out.println(req.getRequestURI());
        String code = req.getParameter("code");
        
        if(code != null && !"".equals(code)) {
        	//System.out.println("code:   "+code);
            String get_user_accessToken = systemServiceImp.getProperty_key("get_user_accessToken");
            String codestr = systemServiceImp.getProperty_key("codestr");
            get_user_accessToken = get_user_accessToken.replaceAll(codestr, code);
            
            //获取用户信息
            StringBuffer buffer = systemServiceImp.HttpsSend(get_user_accessToken, "GET",null);
            //System.out.println(buffer.toString());
            JSONObject jsonObj = JSONObject.fromObject(buffer.toString());
            
            //System.out.println("过滤器openid:    "+jsonObj.getString("openid"));
            //System.out.println("过滤器access_token:    "+jsonObj.getString("access_token"));
            User user = userServiceImp.weiXinRegister(jsonObj.getString("openid"), jsonObj.getString("access_token"));
            
            HttpSession session = ((HttpServletRequest)request).getSession();
            session.setAttribute("user", user);
            //System.out.println(get_user_accessToken);
        }
        
		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
		//两种方式作用一致,此为web方式，本系统采用ApplicationContextAware接口方式
		//wac = (WebApplicationContext) fConfig.getServletContext().getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
		//wac = WebApplicationContextUtils.getWebApplicationContext(fConfig.getServletContext());
		
	}

}
