
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<!doctype html>
<html class="no-js">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>login</title>
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<link rel="stylesheet" href="${basepath}/inc/login/css/app.css">
<style type="text/css">
.text {
	float: right;
	margin-right: 10px;
	line-height: 20px;
	padding-top: 5px;
}

.wzcolor {
	color: #c3c3a9;
}

.wlsignin {
	margin-top: 20px;
	color: #ff6a4c;
}

.icon {
	color: #ff6a4c;
}

.veryfy {
	border-radius: 30px !important;

}
</style>
</head>
<body>
	<div class="am-g">
		<!-- LOGO -->
		<div class="am-u-sm-12 am-text-center">
			<div class='wlsignin'>
				<h2>欢迎注册</h2>
			</div>
		</div>
		<!-- 登陆框 -->
		<div class="am-u-sm-11 am-u-sm-centered">
			<form class="am-form">
				<fieldset class="myapp-login-form am-form-set">
					<div class="am-form-group am-form-icon">
						<i class="am-icon-phone"></i> <input type="text" 
							class="myapp-login-input-text am-form-field " id="phonenumber" placeholder="输入手机号">
					</div>
					<div class="am-form-group am-form-icon">
						<i class="am-icon-lock"></i> <input type="password" id="onepassword"
							class="myapp-login-input-text am-form-field" placeholder="输入密码">
					</div>
					<div class="am-form-group am-form-icon">
						<i class="am-icon-lock"></i> <input type="password" id="twopassword"
							class="myapp-login-input-text am-form-field" placeholder="输入再次密码">
					</div>
					
					<div class="am-input-group veryfy">
						<input type="text" class="am-form-field phoneconfirm" id="phoneconfirm" > 
						      <span class="am-input-group-btn">
                        		<button class="am-btn am-btn-default getvarifacation" type="button">获取验证码</button>
      						</span>
					</div>
				</fieldset>
				<button class="myapp-login-form-submit am-btn am-btn-primary am-btn-block " id="signin">注册</button>
			</form>
		</div>

	</div>

	<!--[if (gte IE 9)|!(IE)]><!-->
	<script src="${basepath}/inc/login/js/jquery.min.js"></script>
	<!--<![endif]-->
	<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->
	<script src="${basepath}/inc/login/js/amazeui.min.js"></script>
	<script src="${basepath}/inc/login/js/app.js"></script>
<script type="text/javascript">
		$(function(){
			$(".getvarifacation").click(function(){
				var phonenumber=$("#phonenumber").val();
				if(phonenumber=="" || phonenumber.length!=11){
					alert("手机号码输入错误");
					return false;
				}else{
					$.post("${basepath}/user/verification/register.znck",{
						phone : phonenumber
					},function(data,textStatus){
						var phone = data;
						console.log(data);
					});
				}
			});
			$("#signin").click(function(event){
				var onepassword=$("#onepassword").val();
				var twopassword=$("#twopassword").val();
				var phoneconfirm=$("#phoneconfirm").val();
			if(onepassword!=twopassword || onepassword=="" || twopassword==""){
				alert("两次密码不一样或者密码为空");
				return false;
			}
			if(phoneconfirm==""){
				alert("验证码为空");
				return false;
			}
			
			});
		});
	</script>
</body>
</html>