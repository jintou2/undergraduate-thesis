<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="utf-8"/>
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>
<script>
$(function(){
	$('.mustCarId').blur(function(){
		var $parent = $(this).parent();
		$parent.find('.error').remove();
		if($(this).is('.mustCarId')){
			var express = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$/;
			if(this.value == "" || express.test(this.value)==false){
				$parent.append('<span class="error">'+"请输入正确的车牌"+'</span>');
			}
		}
	});
});
</script>
<style>
.error{
	color:red;	
}
body{
width:100%;
}
.contain{
	width:100%;
}
.carInformation{
	background-color: #F5F5F5;
	color: #337AB7;
	text-align:center;
	font-size: 20px;
	font-weight:bold;
	padding:10px;
}
.addOneCar{
	margin:5px;
	border-bottom:1px solid white;
}
.addCar{
	margin: 20px auto;
	font-size: 25px;
	font-weight:bold;
	text-align:center;
	border:1px dotted #96B97D;
	padding: 30px;
}
.atention{
	background-color: #F5F5F5;
	color: #337AB7;
}
.carId{
	font-size: 18px;
	text-align:center;
	padding: 15px;
}
.carTitle{
	border-bottom:1px solid #F5F5F5;
}
.carType{
	font-size: 18px;
	text-align:center;
	padding: 15px;
}
.addCertifiction{
	margin: 10px;
	padding:10px;
	border-bottom:2px solid #F5F5F5;
}
.setButton{
	height: 40px;
}
.selectCarType{
	padding: 15px;
}
.carCert{
	padding: 10px;
	font-size: 18px;
	text-align:center;
}
.atention{
	padding:10px;
}
.submit{
	margin: 0px 5px;
	font-size: 18px;
	font-weight: bold;
}
.mustInputCarId{
	margin:10px;
}
</style>
</head>
<body>
<div class="contain">

<div class="carInformation">
填写车辆信息
</div>
		<div>
			<div class="carTitle">
				<div class="row">
					<div class="col-xs-4 carId">车牌号码</div>
					<div class="col-xs-5 mustInputCarId">
						<input type="text" class="form-control mustCarId" placeholder="必修填写车牌号">
					</div>
				</div>
			</div>
			<div>
				<div class="row">
					<div class="col-xs-4 carType">车辆类型</div>
					<div class="col-xs-5">
						<div class="form-group selectCarType">
							<select id="disabledSelect" class="form-control">
								<option>大车</option>
								<option>校车</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="atention">注意信息:请上传真实信息,选择性上传</div>
			<div class="addCertifiction">
				<div class="row">
					<div class="col-xs-4 carCert">上传证件</div>
					<div class="col-xs-6">
						<input type="file">
					</div>
				</div>
			</div>
			<div>
				<button type="submit" class="btn btn-block submit">提交</button>
			</div>
		</div>

	</div>
</body>
</html>