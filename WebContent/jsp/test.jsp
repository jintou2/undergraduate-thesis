<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Graphs</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!-- Bootstrap Core CSS -->
<link href="${basepath}/inc/template/css/bootstrap.min.css"
	rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="${basepath}/inc/template/css/style.css" rel='stylesheet' type='text/css' />
<link href="${basepath}/inc/template/css/font-awesome.css" rel="stylesheet">
<!-- jQuery -->
<script src="${basepath}/inc/template/js/jquery.min.js"></script>
<!---//webfonts--->
<!-- chart -->
<!-- Nav CSS -->
<link href="${basepath}/inc/template/css/custom.css" rel="stylesheet">
<!-- Metis Menu Plugin JavaScript -->
<script src="${basepath}/inc/template/js/metisMenu.min.js"></script>
<script src="${basepath}/inc/template/js/custom.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${basepath}/inc/template/js/bootstrap.min.js"></script>

<script type="text/javascript">
	
</script>
<!-- //chart -->
<style>
.tablepan {
	margin-left: 120px;
	margin-right: 120px;
}

.table {
	margin-bottom: 10px;
}

.incolor {
	width: 80%;
	border-radius: 10px;
	margin-bottom: 0px;
	height: 30px;
}

.panel-footer {
	background: white;
}

input {
	border-color: #F2F4F8;
}

.dis {
	margin-bottom: 2px;
}

.form-inline {
	margin-bottom: 5px;
	margin-left: 15px;
}

.form-control1 {
	width: 30%;
	margin-bottom: 2px;
}

.onetitle {
	width: 250px;
}

.input-dyna-add {
	margin-left: 20px;
	margin-bottom: 5px;
}

.sub {
	margin-left: 20px;
	margin-bottom: 5px;
}
</style>
<script type="text/javascript">
	$(function() {

		$('.input-dyna-add').click(function() {
							var input = $(" <div class='input-group form-inline'><input type='text' class='form-control1' style='width:30%;margin-bottom:2px;' name='name[]' placeholder='请输入名字'><input type='text' class='form-control1' style='width:30%;margin-bottom:2px;' name='name[]' placeholder='请输入名字'> <button class='removeclass btn btn-default' type='button'><span class='glyphicon glyphicon-minus'></span></button></div>'");
							// append 表示添加在标签内， appendTo 表示添加在元素外尾
							$(".firstdiv").append(input);
						});
		$("body").on("click", ".removeclass", function(e) {
			// remove text box  
			$(this).parent('div').remove();
			return false;
		});
		function  getMenuString() {
			var varArr = new Array;
			var view;
			$("input[name='name[]']").each(function(index, item) {
				varArr[index] = $(this).val();
				var priv = varArr.join(',');
				console.log(priv);
				view ={
						"button" : [ {
							"name" : varArr[0],
							"sub_button" : [ {
								"type" : "view",
								"name" : varArr[1],
								"url" : varArr[2],
								"sub_button": [ ]
							} ]
						}, {
							"name" : varArr[3],
							"sub_button" : [ {
								"type" : "view",
								"name" : varArr[4],
								"url" : varArr[5],
								"sub_button": [ ]
							},{
								"type" : "view",
								"name" : varArr[6],
								"url" : varArr[7],
								"sub_button": [ ]
							},{
								"type" : "view",
								"name" : varArr[8],
								"url" : varArr[9],
								"sub_button": [ ]
							},{
								"type" : "view",
								"name" : varArr[10],
								"url" : varArr[11],
								"sub_button": [ ]
							},{
								"type" : "view",
								"name" : varArr[12],
								"url" : varArr[13],
								"sub_button": [ ]
							} ]
						}, {
							"name" : varArr[14],
							"sub_button" : [ {
								"type" : "view",
								"name" : varArr[15],
								"url" : varArr[16],
								"sub_button": [ ]
							},{
								"type" : "view",
								"name" : varArr[17],
								"url" : varArr[18],
								"sub_button": [ ]
							},{
								"type" : "view",
								"name" : varArr[19],
								"url" : varArr[20],
								"sub_button": [ ]
							},{
								"type" : "view",
								"name" : varArr[21],
								"url" : varArr[22],
								"sub_button": [ ]
							} ]
						} ]
					
					}
				console.log(view);
				
			});
			return view;
			}
			
			$(".sub").click(function(){
				var menu = getMenuString();
				var viewto=JSON.stringify(menu);
				alert(viewto);
				//$.post("${basepath}/weixin/menu/modify.znck",
					//	{ menujson : viewto},
						//function(data,testStatus){
							//var obj = JSON.parse(data);
							//if(obj.result==true){
							//window.location.href ="${basepath}/weixin/menu.znck";
							//}
						//});
			});
			
		
	});
</script>
</head>
<div id="wrapper"></div>
<!-- Navigation -->
<nav class="top1 navbar navbar-default navbar-static-top"
	role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="index.html">Modern</a>
	</div>

	<form class="navbar-form navbar-right">
		<input type="text" class="form-control" value="Search..."
			onfocus="this.value = '';"
			onblur="if (this.value == '') {this.value = 'Search...';}">
	</form>
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li><a href="index.html"><i
						class="fa fa-dashboard fa-fw nav_icon"></i>Dashboard</a></li>
				<li><a href="#"><i class="fa fa-laptop nav_icon"></i>Layouts<span
						class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="grids.html">Grid System</a></li>
					</ul> <!-- /.nav-second-level --></li>
				<li><a href="#"><i class="fa fa-indent nav_icon"></i>微信界面控制<span
						class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="graphs.html">查看界面</a></li>
						<li><a href="typography.html">增加界面</a></li>
					</ul> <!-- /.nav-second-level --></li>
				<li><a href="#"><i class="fa fa-envelope nav_icon"></i>Mailbox<span
						class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="inbox.html">Inbox</a></li>
						<li><a href="compose.html">Compose email</a></li>
					</ul> <!-- /.nav-second-level --></li>
				<li><a href="widgets.html"><i class="fa fa-flask nav_icon"></i>Widgets</a>
				</li>
				<li><a href="#"><i class="fa fa-check-square-o nav_icon"></i>Forms<span
						class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="forms.html">Basic Forms</a></li>
						<li><a href="validation.html">Validation</a></li>
					</ul> <!-- /.nav-second-level --></li>
				<li><a href="#"><i class="fa fa-table nav_icon"></i>Tables<span
						class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="basic_tables.html">Basic Tables</a></li>
					</ul> <!-- /.nav-second-level --></li>
				<li><a href="#"><i class="fa fa-sitemap fa-fw nav_icon"></i>Css<span
						class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="media.html">Media</a></li>
						<li><a href="login.html">Login</a></li>
					</ul> <!-- /.nav-second-level --></li>
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
	 	 <div class="graphs"></div>
	<div class="panel panel-default tablepan">
		<div class="panel-body">第一列导航栏</div>
		<div class="panel-footer firstdiv">
			<div class='col-md-5 grid_box1 dis'>
				<input type="text" class="form-control1 onetitle" name='name[]' placeholder="一级菜单名称"></input>
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]' placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
		</div>

	</div>
	<div class="panel panel-default tablepan">
		<div class="panel-body">第二列导航栏</div>
		<div class="panel-footer firstdiv">
			<div class='col-md-5 grid_box1 dis'>
				<input type="text" class="form-control1 onetitle" name='name[]'placeholder="一级菜单名称">
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]' placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]'  placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]'  placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]'  placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]' placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
		</div>

	</div>
		<div class="panel panel-default tablepan">
		<div class="panel-body">第三列导航栏</div>
		<div class="panel-footer firstdiv">
			<div class='col-md-5 grid_box1 dis'>
				<input type="text" class="form-control1 onetitle" name='name[]'placeholder="一级菜单名称">
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]' placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]'  placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]'  placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
			<div class='input-group form-inline'>
				<input type='text' class='form-control1' name='name[]'  placeholder='请输入名称'> 
			    <input type='text' class='form-control1' name='name[]' placeholder='请输入url'>
			</div>
		</div>

	</div>
 			 <div class="graphs">	
 			 	<button type="button" class="btn btn-success btn-block sub">提交</button>		
			</div>

</div>


</body>
</html>
