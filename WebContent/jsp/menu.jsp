<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>
<body>
<c : forEach items="${jsonObj}" var="menu" varStatus="status">
	<c : out>${ menu.name.button[0].name}</c : out>
</c : forEach>
</body>
</html>