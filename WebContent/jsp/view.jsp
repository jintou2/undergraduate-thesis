<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="utf-8"/>
</head>
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>
<style>
.tablepan{
	margin-left:120px;
	margin-right: 120px;
	 
}
.table{
	margin-bottom: 10px;
}
</style>
<script type="text/javascript">
	$(function() {
		//删除菜单栏
		$(".deletview").click(function(){
			$.get('${basepath}/weixin/menu/delete.znck',function(data,testStatus){
				console.log(data);
			});
		});
		$(".check").click(function() {
							$.get("${basepath}/weixin/menu/query.znck",
										function(data, textStatus) {
												console.log("data is" + data);
												var queryjson = JSON.parse(data);

												var name = queryjson.menu;
												console.log("name is" + name);
												var length = name.button.length;
												//长度
												console.log("length is"
														+ length);
												var bt = name.button[0].name;

												console.log("name is" + bt);
												var bt = name.button[0].sub_button[0].type;
												console.log("name is" + bt);
												//sub_button的长度
												var subbtlength = name.button[0].sub_button.length;
												var subbtlength1 = name.button[1].sub_button.length;
												var subbtlength2 = name.button[2].sub_button.length;
												
												//遍历一级菜单
												var title = [];
												var subname = [];

												for (var i = 0; i < length; i++) {
													title[i] = name.button[i].name;
													console.log("title is"
															+ title[i]);
												}
												//一级菜单的第一个
											 	/*for (var i = 0; i < name.button[0].sub_button.length; i++) {
													
													$("#two").append("<th>"+ name.button[0].sub_button[0].type+ "</th>");
													$("#two").append("<th>"+ name.button[0].sub_button[0].name + "</th>");
													$("#two").append("<th>"+ name.button[0].sub_button[0].url + "</th>");
												}*/
												var data="";
												for (var i = 0; i < name.button[0].sub_button.length; i++) {
													data +="<tr>"+
													"<td>"+name.button[0].sub_button[i].type+"</td>"+
													"<td>"+name.button[0].sub_button[i].name+"</td>"+
													"<td>"+name.button[0].sub_button[i].url+"</td>"+
													"</tr>";
												}
												$("#fort").append(data);
												//第二个一级菜单
												var datatwo="";
												for(var i=0;i<name.button[1].sub_button.length;i++){
													datatwo +="<tr>"+
													"<td>"+name.button[1].sub_button[i].type+"</td>"+
													"<td>"+name.button[1].sub_button[i].name+"</td>"+
													"<td>"+name.button[1].sub_button[i].url+"</td>"+
													"</tr>";
												}
												//第三个菜单
												var datathree="";
												for(var i=0;i<name.button[2].sub_button.length;i++){
													datathree +="<tr>"+
													"<td>"+name.button[2].sub_button[i].type+"</td>"+
													"<td>"+name.button[2].sub_button[i].name+"</td>"+
													"<td>"+name.button[2].sub_button[i].url+"</td>"+
													"</tr>";
												}
												$("#forttwo").append(datatwo);
												$("#forthree").append(datathree);
												
												$("#titleone").html(title[0]);
												$("#titeltwo").html(title[1]);
												console.log("title is"+ title[1]);
												$("#titlethree").html(title[2]);
											});
						})
	});
</script>
<body>
	<button type="button" class="btn btn-primary check">查看微信菜单栏</button>
	<button type="button" class="btn btn-primary deletview">删除菜单栏</button>
	<div class="panel panel-default tablepan">
		<div class="panel-body">第一列导航栏</div>
		<div class="panel-footer">
			<table class="table">
				<thead id="tb">
					<th>key的值</th>
					<th>name值</th>
					<th>url值</th>
				</thead>
				<tbody id="fort">
					<tr id="one">
						<th>一级菜单</th>
						<th id="titleone"></th>
						<th></th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="panel panel-default tablepan">
	<div class="panel-body">第二列导航栏</div>
	<div class="panel-footer">
 	<table class="table">
		<thead>
			<th>key的值</th>
			<th>name值</th>
			<th>url值</th>
		</thead>
		<tbody id="forttwo">
			<tr id="">
				<td>一级菜单</td>
				<td id="titeltwo"></td>
				<td></td>
			</tr>
			
		</tbody>
	</table>
	</div>
	</div>
	
	<div class="panel panel-default tablepan">
	<div class="panel-body">第三列导航栏</div>
	<div class="panel-footer">
	<table class="table">
		<thead>
			<th>key的值</th>
			<th>name值</th>
			<th>url值</th>
		</thead>
		<tbody id="forthree">
			<tr id="">
				<td>一级菜单</td>
				<td></td>
				<td></td>
			</tr>
			
		</tbody>
	</table>
	</div>
	</div>
	
</body>
</html>