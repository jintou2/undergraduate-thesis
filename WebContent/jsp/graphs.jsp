<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Graphs</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="${basepath}/inc/template/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="${basepath}/inc/template/css/style.css" rel='stylesheet' type='text/css' />
<link href="${basepath}/inc/template/css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<script src="${basepath}/inc/template/js/jquery.min.js"></script>
<!---//webfonts--->  
<!-- chart -->
<script src="${basepath}/inc/template/js/Chart.js"></script>
    <!-- Nav CSS -->
<link href="${basepath}/inc/template/css/custom.css" rel="stylesheet">
<!-- Metis Menu Plugin JavaScript -->
<script src="${basepath}/inc/template/js/metisMenu.min.js"></script>
<script src="${basepath}/inc/template/js/custom.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${basepath}/inc/template/js/bootstrap.min.js"></script>

<script type="text/javascript">
	$(function() {
		//删除菜单栏
		$(".deletview").click(function(){
			$.get('${basepath}/weixin/menu/delete.znck',function(data,testStatus){
				console.log(data);
			});
		});
		$(".check").click(function() {
							$.get("${basepath}/weixin/menu/query.znck",
										function(data, textStatus) {
												console.log("data is" + data);
												var queryjson = JSON.parse(data);

												var name = queryjson.menu;
												console.log("name is" + name);
												var length = name.button.length;
												//长度
												console.log("length is"
														+ length);
												var bt = name.button[0].name;

												console.log("name is" + bt);
												var bt = name.button[0].sub_button[0].type;
												console.log("name is" + bt);
												//sub_button的长度
												var subbtlength = name.button[0].sub_button.length;
												var subbtlength1 = name.button[1].sub_button.length;
												var subbtlength2 = name.button[2].sub_button.length;
												
												//遍历一级菜单
												var title = [];
												var subname = [];

												for (var i = 0; i < length; i++) {
													title[i] = name.button[i].name;
													console.log("title is"
															+ title[i]);
												}
												//一级菜单的第一个
											 	/*for (var i = 0; i < name.button[0].sub_button.length; i++) {
													
													$("#two").append("<th>"+ name.button[0].sub_button[0].type+ "</th>");
													$("#two").append("<th>"+ name.button[0].sub_button[0].name + "</th>");
													$("#two").append("<th>"+ name.button[0].sub_button[0].url + "</th>");
												}*/
												var data="";
												for (var i = 0; i < name.button[0].sub_button.length; i++) {
													data +="<tr>"+
													"<td>"+name.button[0].sub_button[i].type+"</td>"+
													"<td>"+name.button[0].sub_button[i].name+"</td>"+
													"<td>"+name.button[0].sub_button[i].url+"</td>"+
													"</tr>";
												}
												$("#fort").append(data);
												//第二个一级菜单
												var datatwo="";
												for(var i=0;i<name.button[1].sub_button.length;i++){
													datatwo +="<tr>"+
													"<td>"+name.button[1].sub_button[i].type+"</td>"+
													"<td>"+name.button[1].sub_button[i].name+"</td>"+
													"<td>"+name.button[1].sub_button[i].url+"</td>"+
													"</tr>";
												}
												//第三个菜单
												var datathree="";
												for(var i=0;i<name.button[2].sub_button.length;i++){
													datathree +="<tr>"+
													"<td>"+name.button[2].sub_button[i].type+"</td>"+
													"<td>"+name.button[2].sub_button[i].name+"</td>"+
													"<td>"+name.button[2].sub_button[i].url+"</td>"+
													"</tr>";
												}
												$("#forttwo").append(datatwo);
												$("#forthree").append(datathree);
												
												$("#titleone").html(title[0]);
												$("#titeltwo").html(title[1]);
												console.log("title is"+ title[1]);
												$("#titlethree").html(title[2]);
											});
						})
	});
</script>
<!-- //chart -->
<style>
.tablepan{
	margin-left:120px;
	margin-right: 120px;
	 
}
.table{
	margin-bottom: 10px;
}
</style>
</head>
<body>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Modern</a>
            </div>
    
			<form class="navbar-form navbar-right">
              <input type="text" class="form-control" value="Search..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search...';}">
            </form>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.html"><i class="fa fa-dashboard fa-fw nav_icon"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-laptop nav_icon"></i>Layouts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="grids.html">Grid System</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-indent nav_icon"></i>微信菜单栏<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="graphs.html">查询</a>
                                </li>
                                <li>
                                    <a href="typography.html">修改</a>
                                </li>
								 <li>
                                    <a href="typography.html">删除</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-envelope nav_icon"></i>Mailbox<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="inbox.html">Inbox</a>
                                </li>
                                <li>
                                    <a href="compose.html">Compose email</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="widgets.html"><i class="fa fa-flask nav_icon"></i>Widgets</a>
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-check-square-o nav_icon"></i>Forms<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="forms.html">Basic Forms</a>
                                </li>
                                <li>
                                    <a href="validation.html">Validation</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-table nav_icon"></i>Tables<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="basic_tables.html">Basic Tables</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw nav_icon"></i>Css<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="media.html">Media</a>
                                </li>
                                <li>
                                    <a href="login.html">Login</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="graphs">
	    <button type="button" class="btn btn-primary check">查看微信菜单栏</button>
	<button type="button" class="btn btn-primary deletview">删除菜单栏</button>
	<div class="panel panel-default tablepan">
		<div class="panel-body">第一列导航栏</div>
		<div class="panel-footer">
			<table class="table">
				<thead id="tb">
					<th>key的值</th>
					<th>name值</th>
					<th>url值</th>
				</thead>
				<tbody id="fort">
					<tr id="one">
						<th>一级菜单</th>
						<th id="titleone"></th>
						<th></th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="panel panel-default tablepan">
	<div class="panel-body">第二列导航栏</div>
	<div class="panel-footer">
 	<table class="table">
		<thead>
			<th>key的值</th>
			<th>name值</th>
			<th>url值</th>
		</thead>
		<tbody id="forttwo">
			<tr id="">
				<td>一级菜单</td>
				<td id="titeltwo"></td>
				<td></td>
			</tr>
			
		</tbody>
	</table>
	</div>
	</div>
	
	<div class="panel panel-default tablepan">
	<div class="panel-body">第三列导航栏</div>
	<div class="panel-footer">
	<table class="table">
		<thead>
			<th>key的值</th>
			<th>name值</th>
			<th>url值</th>
		</thead>
		<tbody id="forthree">
			<tr id="">
				<td>一级菜单</td>
				<td></td>
				<td></td>
			</tr>
			
		</tbody>
	</table>
	</div>
	</div>
		</div>	
	
  

    <!-- /#wrapper -->

</body>
</html>
