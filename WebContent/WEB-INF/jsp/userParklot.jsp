<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<title>我的停车场</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<style>
.parkinglotInformation {
	text-align: center;
	border: 1px solid #FFFFFF; 
	margin: 5px;
	font-size: 20px;
	margin : 5px;
}
a{
	color: black;
}
a:hover{
	text-decoration:none;
	color: black;
}
.car {
	background-color: #dff0d8;
	text-align: center;
	font-size: 20px;
	font-weight: bold;
	color: #3c8584;
	padding: 10px;
}

</style>
<body>
	<div class="contain">
		<div class="car"> 
			<span class="am-icon-cab">我的停车场</span>
		</div>
		<c:if test="${!empty usersParkinglots}">
			<ul class="list-group">
				<c:forEach items="${usersParkinglots}" var="userParklot">
					<a href="${basepath}/userParklot/profile/${userParklot.id}.znck">
						<li class="list-group-item">
							<div>${userParklot.name}停车场</div>
							<div>
								<span>起始价：${userParklot.init_price}*每小时${userParklot.per_hour_price}/元</span>
							</div>
					</li>
					</a>
				</c:forEach>
			</ul>
		</c:if>
	</div>

	<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
	<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>

	<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
	<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script>
		
	</script>
</body>
</html>