<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="zh-CN">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=eN9cBjl262LzV7Q0dVAE0dcmNY4yzO6H"></script>
<style>
body .contain {
	height: 100%;
	font-size: 12px;
	font-family: Arial;
}

.imgAndInfo {
	margin: 10px;
}

div.imgInimgAndInfo {
	margin: 10px auto;
	height: 120px;
	border: 1px solid #F5F5F5;;
}

div.parkName {
	margin: 0px 0px 5px;
	background-color: #F5F5F5;
	border: 1px solid #F5F5F5;
	text-align: center;
	padding: 10px;
}

div.carLocation {
	background-color: #F5F5F5;
}

div.parkStatus {
    border: solid 1px #F5F5F5;
	padding: 10px;
}

.carDistance {
	
}

.carName {
	color: black;
	font-size: 20px;
	font-weight: bold;
}

.carInformation {
	background-color: #F5F5F5;
	margin: 5px 0px;
}

.carTitle {
	font-size: 18px;
	font-weight: bold;
	padding: 10px;
	border: 1px solid white;
	color: #3c8584;
}

.carTitles {

	padding: 15px;
	border: 2px solid  #F5F5F5;
	
}

.carTitle1 {
	font-size: 18px;
	font-weight: bold;
	padding: 5px;
	border: 1px solid white;
}

.productPriceDiv {
	width: 50%;
}

.parkEvaluate {
	background-color: #F5F5F5;
}

.sixTr {
	border: 1px solid #F5F5F5;
	height: 100px;
	text-align: center;
	position: relative;
}

.six {
	text-align: center;
	font-size: 25px;
	color: red;
	margin: 5px;
}

.six2 {
	text-align: center;
	font-size: 25px;
	color: red;
}

.parkInf {
	bolder: 1px solid white;
	text-align: center;
	padding: 10px;
}

.speciction {
	padding: 5px;
	font-size: 8px;
}

.parkSize {
	border: 1px solid white;
	background-color: #F5F5F5;
	width: 100%;
}

.bottom {
	text-align: center;
	background-color: #FFEDED;
	font-size: 16px;
}

.navgation {
	background-color: #FFEDED;
	text-align: center;
	font-size: 16px;
	font-family: arial;
	border: 1px solid white;
}

button {
	background-color: #F5F5F5;
	border: 1px solid transparent;
	color: blue;
	font-size: 20px;
	font-weight: bold;
	padding: 5px 0px;
}

.book {
	border: 1px solid white;
}

.carTitle {
	background-color: #dff0d8;
    padding-bottom: 1px;
}

.carId {
	font-size: 18px;
	text-align: center;
}

.inputMoney {
	width: 20%;
	height: 20%;
	border-style: 1px solid white;
}

.startMoney {
	width: 10%;
	height: 20%;
	border-style: 1px solid white;
}

.parkNUmber {
	border-style: 1px solid white;
}

.car {
	background-color: #F5F5F5;
	text-align: center;
	font-size: 20px;
	font-weight: bold;
	color: #337AB7;
	padding: 10px;
}
.carType{
	background-color:white;
	padding-top: 10px;
	border: 1px solid  #F5F5F5;
}
.container-fluid{
  width:100%;
}
.myOrder {
	font-size: 15px;
	color: #3c8584;
	background-color: #F5F5F5;
	margin-bottom: 20px;
	border: 1px solid white;
}
</style>
</head>
<body>
<div class="contain">
	
	<!-- <div class="am-g">
		<div class=" col-sm-12 col-sm-centered">
			<form class="am-form">
				<fieldset class="am-form-set">
					<input type="text" class="am-form-field am-input-lg"
						placeholder="停车场名称"> <input type="text"
						class="am-form-field am-input-lg" placeholder="停车场位置"> <input
						type="email" placeholder="填下邮箱">

				</fieldset>

			</form>
		</div>
	</div> -->

	<div class="infoInimgAndInfo">
		<div class="car am-cf">
			<span class="am-icon-paypal">修改停车场</span>
			<span class="am-icon-newspaper-o  am-fl"></span>
			<button class="btn btn-primary btn-sm myOrder pull-right mypark">
				<span class="glyphicon glyphicon-user"></span> 我的停车场
			</button>
		</div>
		<div class="carTitles">
			<div class="row">
				<div class="col-xs-4 carId">停车场名称</div>
				<div class="col-xs-8">
					<input type="text" class="form-control carNamess" value="${profile.name}">
				</div>
			</div>
		</div>
		<div class="parkStatus">
			<div class="row">
				<div class="col-xs-4 carId">
					<p>
					<span class="am-icon-location-arrow"></span>
						停车场位置
					 </p>
				</div>
				<div class="col-xs-8">
					<input type="text" class="form-control parkLocation" value="${profile.address}">
				</div>
			</div>

		</div>
		<div class="carInformation">
			<div class="carTitle">
			<p><span class="am-icon-cab"></span> 车位信息</p>
			</div>
			<div class="row parkInf">
				<div class="col-xs-6">
					<span class="sixTr"><span class="six"> 
					<input class="inputMoney" value="${profile.per_hour_price}"></span>元/小时</span>
					<div class="speciction">
						起始价<input class="startMoney" value="${profile.init_price}">元
					</div>
				</div>
				<div class="col-xs-6 col-md-4">
					<span class="sixTr"> <span class="six2 "><input
							class="inputMoney" value="${profile.park_number}">个</span>空车位
					</span>
				</div>
			</div>

			<div class="carType">
				<div class="row">
					<div class="col-xs-5 carId ">车辆类型</div>
					<div class="col-xs-5">
						<div class="form-group selectCarType">
							<select id="disabledSelect" class="form-control">
								<option>大车</option>
								<option>小车</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="productSaleAndReviewNumber">
			<div class="carTitle">
				<p><span class="am-icon-edit"></span> 描述</p>
			</div>

			<textarea class="form-control discription" value="${profile.description}"></textarea>
		</div>
		<button type="button" class="btn btn-info btn-block submit">提交</button>
	</div>
	<div>
	</div>
	<div style="clear: both"></div>
	</div>
</div>

</body>
<script>
	$(function() {
		var map = new BMap.Map();
		var myGeo = new BMap.Geocoder();
		function getPoint(parkLocation) {

		}
		//查看我的停车场
		$(".mypark").click(function(){
			 window.location.href = "${basepath}/addParklot/queryParklot.znck";
		});
		
		$(".submit").click(function() {
			var parkState = 1;
			var parkName = $(".carNamess").val();
			var parkLocation = $(".parkLocation").val();
			var inputMoney = $(".inputMoney").val();
			var startMoney = $(".startMoney").val();
			var parkNumber = $(".parkNumber").val();
			var discription = $(".discription").val();
			var disabledSelect = $("#disabledSelect").val();
			var id = ${profile.id};
			var parkStatus = $("#parkStatus").val();
			myGeo.getPoint(parkLocation, function(point) {
				var lng = point.lng;
				var lat = point.lat;
				
				$.post('${basepath}/userParklot/updateParklot.znck', {
					id: id,
					address: parkLocation,
					name : parkName,
					park_number : parkNumber,
					longitude : lng,
					latitude : lat, 
					per_hour_price : inputMoney,
					init_price : startMoney,
					description : discription,
					surplus_park_num: parkNumber,
					status: parkState
				}, function(data, textStatus) {
					window.location.href = "${basepath}/userParklot/profile/"+id+".znck";
				});
			});
		});
	});
</script>
</html>