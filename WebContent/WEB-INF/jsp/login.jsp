
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<!doctype html>
<html class="no-js">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>login</title>
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<link rel="stylesheet" href="${basepath}/inc/login/css/app.css">
<style type="text/css">
.text {
	float: right;
	margin-right: 10px;
	line-height: 20px;
	padding-top: 5px;
}
.wzcolor{
	color:#c3c3a9;
}
.textColor{
	color:white;
}
a:hover{
	text-decoration:none;
	color: white;
}
</style>
</head>
<body>
	<div class="am-g">
		<!-- LOGO -->
		<div class="am-u-sm-12 am-text-center">
			<i class="am-icon-twitch myapp-login-logo"></i>
		</div>
		<!-- 登陆框 -->
		<div class="am-u-sm-11 am-u-sm-centered">
			<form class="am-form">
				<fieldset class="myapp-login-form am-form-set">
					<div class="am-form-group am-form-icon">
						<i class="am-icon-user"></i>
						 <input type="text" class="myapp-login-input-text am-form-field" id="phone"
							placeholder="请输入您的账号">
					</div>
					<div class="am-form-group am-form-icon">
						<i class="am-icon-lock"></i> <input type="password"
							class="myapp-login-input-text am-form-field" id="inputPassword"
							placeholder="输入密码">
					</div>
					<div class="checkbox wzcolor">
						<label><input type="checkbox">Remember me</label> 
						<span class='text '>Forget Password?</span>
					</div>
				</fieldset>
				<button type="button" class="myapp-login-form-submit am-btn am-btn-primary am-btn-block loginin">登陆</button>
				<button class="myapp-login-form-submit am-btn am-btn-primary am-btn-block signin">
					<a href="${basepath}/user/view/register.znck" class="textColor">注册</a>
				</button>
				
			</form>
		</div>

	</div>

	<!--[if (gte IE 9)|!(IE)]><!-->
	<script src="${basepath}/inc/login/js/jquery.min.js"></script>
	<!--<![endif]-->
	<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->
	<script src="${basepath}/inc/login/js/amazeui.min.js"></script>
	<script src="${basepath}/inc/login/js/app.js"></script>
	<script type="text/javascript">
		$(function() {
			$(".loginin").click(function() {
				var phone = $("#phone").val();
				var inputPassword = $("#inputPassword").val();
				if (phone == "" || inputPassword == "") {
					alert("手机号或者密码为空");
					return false;
				} else {
					$.post('${basepath}/user/phone/login.znck', {
						Phone : phone,
						Password : inputPassword
					}, function(data, textStatus) {
						console.log(data);
						var obj = JSON.parse(data);
						if(obj.result==true){
							  window.location.href='${basepath}/parklots.znck';
							//window.location.href="www.baidu.com";
							//window.location.href="${basepath}/parklots.znck";
							return true;
						}
						else{
							alert("登陆失败,账号或者密码错误！");
						}
					});
				}
			});
		});
	</script>
</body>
</html>