
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<!doctype html>
<html class="no-js">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>投诉</title>
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css"
	rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<link rel="stylesheet" href="${basepath}/inc/login/css/app.css">
<style type="text/css">
.text {
	float: right;
	margin-right: 10px;
	line-height: 20px;
	padding-top: 5px;
}

.wzcolor {
	color: #c3c3a9;
}

.textColor {
	color: white;
}

body {
	background-color: #F5F5F5;
}

#doc-ipt-error {
	
}

.complain {
	text-align: center;
}

.car {
	background-color: #dff0d8;
	text-align: center;
	font-size: 20px;
	font-weight: bold;
	color: #3c8584;
	padding: 10px;
}
</style>
</head>
<body>
	<div class="contain">
		<form role="form">
			<div class="car">
				<span class="">投诉</span>
			</div>
			<div class="form-group">
				<textarea class="form-control" rows="5" placeholder="请输入投诉内容"></textarea>
			</div>
		</form>
		<button type="button" class="btn btn-default btn-lg btn-block compalainText">提交</button>
	</div>
</body>
<script>
$(function(){
	$(".compalainText").click(function(){
		alert("已被提交！请等待回复");
	});
});
</script>
</html>