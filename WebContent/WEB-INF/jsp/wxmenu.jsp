<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Graphs</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">	
addEventListener("load", function() { 
	setTimeout(hideURLbar, 0); }, false); 
	function hideURLbar(){ 
		window.scrollTo(0,1); 
		} 
</script>
<!-- Bootstrap Core CSS -->
<link href="${basepath}/inc/template/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="${basepath}/inc/template/css/style.css" rel='stylesheet' type='text/css' />
<link href="${basepath}/inc/template/css/font-awesome.css" rel="stylesheet">
<!-- jQuery -->
<script src="${basepath}/inc/template/js/jquery.min.js"></script>
<!----webfonts--->
<link href='http://fonts.useso.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<!---//webfonts--->
<!-- chart -->
<script src="${basepath}/inc/template/js/Chart.js"></script>
<!-- Nav CSS -->
<link href="${basepath}/inc/template/css/custom.css" rel="stylesheet">
<!-- Metis Menu Plugin JavaScript -->
<script src="${basepath}/inc/template/js/metisMenu.min.js"></script>
<script src="${basepath}/inc/template/js/custom.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${basepath}/inc/template/js/bootstrap.min.js"></script>

<script type="text/javascript">
	$(function() {
		//删除菜单栏
		$(".deletview").click(
				function() {
					$.get('${basepath}/weixin/menu/delete.znck', 
							function(data,testStatus) {
						console.log(data);
					});
				});
		$("#check").click(function() { $.get("${basepath}/weixin/menu/query.znck",
											function(data, textStatus) {
												var queryjson = JSON.parse(data);
												var name = queryjson.menu;
												console.log("name is" + name);
												var firtMenulength = name.button.length;							
												if($(".panel-warning").length>0){
													$(".panel-warning").remove();
												}
												<!--处理复杂数据-->
												for(var i = 0; i<firtMenulength; i++){
													var data = "";
													var pageWrapper = $(".graphs");
													var firstDiv = $("<div class=\"panel panel-warning\">"+
															"<div class=\"panel-heading\">"+"<h2>"+ "一级菜单："+ name.button[i].name+"</h2>"+ "</div>"
															+ "<div class=\"panel-body no-padding\" style=\"display: block;\">"+
															"<table class=\"table table-striped\"><thead><tr class=\"warning\">"+
															"<th>key值</th><th>name值</th><th>url值</th></tr></thead>"+
															"<tbody></tbody></table></div>"
															+"</div>");
													pageWrapper.append(firstDiv);
													for (var second = i; second < name.button[i].sub_button.length; second++) {
														data += "<tr>"
																+ "<td>"
																+ name.button[i].sub_button[second].type
																+ "</td>"
																+ "<td>"
																+ name.button[i].sub_button[second].name
																+ "</td>"
																+ "<td>"
																+ name.button[i].sub_button[second].url
																+ "</td>" + "</tr>";
													}
													$("tbody").append(data);
												}
											});
						})
	});
</script>
<!-- //chart -->
<style>
.tablepan {
	margin-left: 120px;
	margin-right: 120px;
}

.table {
	margin-bottom: 10px;
}
.editMenuCenter{
	margin-right: 50%;
}
.col-center-block {  
    float: none;  
    display: block;  
    margin-left: auto;  
    margin-right: auto;  
}  
</style>
</head>
<body>

	<%@ include file="common/header.jsp"%>
	<div id="page-wrapper">
		<div class="graphs">
			<div class="grid_3 grid_5">
				<div class="row">
					<div class="col-xs-4 ">
						<button class="btn btn_5 btn-lg btn-info mx-auto col-center-block" id="check"><i class="fa fa-info-circle"></i> 查看微信菜单栏</button>
					</div>
					<div class="col-xs-4">
						<button type="button" class="btn btn_5 btn-lg btn-warning warning_11 edit editMenuCenter col-center-block">
							<span class="glyphicon glyphicon-edit"></span> 编辑微信菜单栏
						</button>
					</div>
					<div class="col-xs-4">
						<button type="button" class="btn btn_5 btn-lg btn-danger col-center-block">
							<span class="glyphicon glyphicon-trash"></span> 删除菜单栏
						</button>
					</div>
			</div>
		</div>
	</div>
  </div>

	<!-- /#wrapper -->
</body>
</html>
