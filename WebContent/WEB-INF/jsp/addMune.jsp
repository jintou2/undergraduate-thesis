<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<title>编辑菜单</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!-- Bootstrap Core CSS -->
<link href="${basepath}/inc/template/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="${basepath}/inc/template/css/style.css" rel='stylesheet' type='text/css' />
<link href="${basepath}/inc/template/css/font-awesome.css" rel="stylesheet">
<!-- jQuery -->
<script src="${basepath}/inc/template/js/jquery.min.js"
	type="text/javascript"></script>
<!---//webfonts--->
<!-- chart -->

<!-- Nav CSS -->
<link href="${basepath}/inc/template/css/custom.css" rel="stylesheet">
<!-- Metis Menu Plugin JavaScript -->
<script src="${basepath}/inc/template/js/metisMenu.min.js" type="text/javascript"></script>
<script src="${basepath}/inc/template/js/custom.js" type="text/javascript"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${basepath}/inc/template/js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript">
	
</script>
<!-- //chart #f2f4f8;-->
<style type="text/css">
.col-center-block {
	display: block;
	margin-left: auto;
	margin-right: auto;
}

.inputType {
	width: 40%;
	margin-bottom: 3px;
}

.topMenuPadding {
	padding-left: 0px;
}

.nodisplay {
	display: none;
}
.show{
	display: block;
}
.deletePanel{
	float: right;
	margin: 0px auto;
}
.trashSet{
	margin-top: -10px;
	background-color: #3799de;
	 border: none;
    outline: none;
}
</style>
</head>
<%@ include file="common/header.jsp"%>
<div id="page-wrapper">
	<div class="graphs">
		<div class="grid_3 grid_5">
			<div class="row">
				<div class="col-xs-4">
					<button type="button"
						class="btn btn_5 btn-lg btn-danger col-center-block">
						<span class="glyphicon glyphicon-trash"></span> 删除菜单栏
					</button>
				</div>
				<div class="col-xs-4 ">
					<button class="btn btn_5 btn-lg btn-info mx-auto col-center-block"
						id="add">
						<span class="glyphicon glyphicon-edit"></span>添加一级菜单
					</button>
				</div>
				<div class="col-xs-4">
					<button type="button" class="btn btn_5 btn-lg btn-warning warning_11 edit editMenuCenter col-center-block">
						<i class="fa fa-check" aria-hidden="true"></i> 提交
					</button>
				</div>

			</div>
		</div>
		
		<!-- 菜单栏目1的处理 -->
		
		<div class="display">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<span>菜单栏一</span> 
					<span class = "deletePanel">
					<button class ="trashSet">
					<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i> 
					</button>   	
   					</span>
					<div class="panel-ctrls">
						<span class="button-icon has-bg"> <i
							class="ti ti-angle-down"></i>
						</span>
					</div>
				</div>
				<div class="panel-body no-padding" style="display: block;">
					<div class="resetMenu">
						<button class="btn btn_5 btn-lg btn-danger resetMenu">重置</button>
					</div>
					<div class="input-group form-inline  input-group-lg" id="defaultInput" >
						<input type='text' class='form-control' style='width: 40%; margin-bottom: 2px;' name='desc[]' placeholder='请输入一级菜单名称'> 
							<input type='text' name='price[]' class='form-control' style='width: 40%; margin-bottom: 2px;' placeholder='请输入一级菜单链接'>
						<button class='btn btn-lg addSecondeMenu' >
							<span class='glyphicon glyphicon-plus'></span>
						</button>
					</div>
					<div class="nodisplay" >
						<div class='col-md-5  topMenuPadding input-group-lg'>
							<input type="text" class="form-control" name='name[]' placeholder="一级菜单名称">
						</div>
						<div class="input-group form-inline  input-group-lg">
							<input type='text' class='form-control' style='width: 40%; ' name='name[]' placeholder='请输入名称'> 
								<input type='text' class='form-control' style='width: 40%; ' name='name[]' placeholder='请输入url'>
							<button class='btn btn-lg addclass'>
								<span class='glyphicon glyphicon-plus'></span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
				<!-- 菜单栏目2的处理 -->
		
		<div class="display">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<span>菜单栏一</span> 
					<span class = "deletePanel">
					<button class ="trashSet">
					<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i> 
					</button>   	
   					</span>
					<div class="panel-ctrls">
						<span class="button-icon has-bg"> <i
							class="ti ti-angle-down"></i>
						</span>
					</div>
				</div>
				<div class="panel-body no-padding" style="display: block;">
					<div class="resetMenu">
						<button class="btn btn_5 btn-lg btn-danger resetMenu">重置</button>
					</div>
					<div class="input-group form-inline  input-group-lg" id="defaultInput" >
						<input type='text' class='form-control' style='width: 40%; margin-bottom: 2px;' name='desc[]' placeholder='请输入一级菜单名称'> 
							<input type='text' name='price[]' class='form-control' style='width: 40%; margin-bottom: 2px;' placeholder='请输入一级菜单链接'>
						<button class='btn btn-lg addSecondeMenu' >
							<span class='glyphicon glyphicon-plus'></span>
						</button>
					</div>
					<div class="nodisplay" >
						<div class='col-md-5  topMenuPadding input-group-lg'>
							<input type="text" class="form-control" name='name[]' placeholder="一级菜单名称">
						</div>
						<div class="input-group form-inline  input-group-lg">
							<input type='text' class='form-control' style='width: 40%; ' name='name[]' placeholder='请输入名称'> 
								<input type='text' class='form-control' style='width: 40%; ' name='name[]' placeholder='请输入url'>
							<button class='btn btn-lg addclass'>
								<span class='glyphicon glyphicon-plus'></span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
				<!-- 菜单栏目3的处理 -->
		
		<div class="display">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<span>菜单栏一</span> 
					<span class = "deletePanel">
					<button class ="trashSet">
					<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i> 
					</button>   	
   					</span>
					<div class="panel-ctrls">
						<span class="button-icon has-bg"> <i
							class="ti ti-angle-down"></i>
						</span>
					</div>
				</div>
				<div class="panel-body no-padding" style="display: block;">
					<div class="resetMenu">
						<button class="btn btn_5 btn-lg btn-danger resetMenu">重置</button>
					</div>
					<div class="input-group form-inline  input-group-lg" id="defaultInput" >
						<input type='text' class='form-control' style='width: 40%; margin-bottom: 2px;' name='desc[]' placeholder='请输入一级菜单名称'> 
							<input type='text' name='price[]' class='form-control' style='width: 40%; margin-bottom: 2px;' placeholder='请输入一级菜单链接'>
						<button class='btn btn-lg addSecondeMenu' >
							<span class='glyphicon glyphicon-plus'></span>
						</button>
					</div>
					<div class="nodisplay" >
						<div class='col-md-5  topMenuPadding input-group-lg'>
							<input type="text" class="form-control" name='name[]' placeholder="一级菜单名称">
						</div>
						<div class="input-group form-inline  input-group-lg">
							<input type='text' class='form-control' style='width: 40%; ' name='name[]' placeholder='请输入名称'> 
								<input type='text' class='form-control' style='width: 40%; ' name='name[]' placeholder='请输入url'>
							<button class='btn btn-lg addclass'>
								<span class='glyphicon glyphicon-plus'></span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<script type="text/javascript">
	$(function() {
		//添加安虐，点击添加之后，去除所有输入，重新添加
		$(".addSecondeMenu").click(function() {
			$(this).parent('div').next().attr("class","show");
			$(this).parent('div').addClass('nodisplay');
		});
		//添加输入框
		$(".addclass").click(function(){
			var input = $(" <div class='input-group form-inline  input-group-lg'>"+
					"<input type='text' class='form-control' style='width:40%;' name='name[]' placeholder='请输入名字'>"+
					"<input type='text' class='form-control' style='width:40%;' name='name[]' placeholder='请输入名字'>"+
					" <button class='removeclass btn btn-lg' >"+
					"<span class='glyphicon glyphicon-minus'></span>"+
					"</button></div>'");
			// append 表示添加在标签内， appendTo 表示添加在元素外尾
			$(this).parent().after(input);
		});
		
		//删除安虐
		$("body").on("click", ".removeclass", function(e) {
			// remove text box  
			$(this).parent().remove();
		});
		
		//重置安虐
		$(".resetMenu").click(function(){
			$(this).parent('div').next().removeClass('nodisplay');
			var nodisplay = $(this).parent('div').next().next();
			nodisplay.attr('class','nodisplay');
		});
		
		//移除panel
		$(".deletePanel").click(function(){
			$(this).parent().next().removeClass('nodisplay');
		});

	});
</script>