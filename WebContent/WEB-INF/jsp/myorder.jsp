<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="zh-CN">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css"
	rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>
<script type="text/javascript"
	src="http://api.map.baidu.com/api?v=2.0&ak=eN9cBjl262LzV7Q0dVAE0dcmNY4yzO6H"></script>
<style>
body {
	font-size: 12px;
	font-family: Arial;
}

.imgAndInfo {
	margin: 10px;
}

div.imgInimgAndInfo {
	margin: 10px auto;
	height: 120px;
	border: 1px solid #F5F5F5;;
}

.carName {
	color: black;
	font-size: 20px;
	font-weight: bold;
}

.carInformation {
	background-color: #F5F5F5;
	margin: 5px 0px;
}

.carTitle {
	font-size: 18px;
	font-weight: bold;
	padding: 10px;
	border: 1px solid white;
}

.parkInf {
	bolder: 1px solid white;
	text-align: center;
	padding: 20px;
}

.speciction {
	padding: 5px;
	font-size: 8px;
}

.parkSize {
	border: 1px solid white;
	background-color: #F5F5F5;
	width: 100%;
}

.bottom {
	text-align: center;
	background-color: #FFEDED;
	font-size: 16px;
}

button {
	border: 1px solid #C40000;
	padding: 5px;
	margin: 10px;
	align: right;
	background-color: white;
	color: #C40000;
}

.carTitle {
	background-color: #F5F5F5;
}

.carId {
	font-size: 18px;
	text-align: center;
}

.car {
	background-color: #dff0d8;
	text-align: center;
	font-size: 20px;
	font-weight: bold;
	color: #3c8584;
	padding: 10px;
}

.carTitless {
	font-size: 18px;
	padding: 10px;
	border: 1px solid white;
	padding-top: 10px;
	background-color: #F5F5F5;
	text-align: center;
}

.carTitle {
	background-color: #dff0d8;
	padding-bottom: 1px;
}

.arriveTime {
	border: 2px solid #F5F5F5;
	padding: 5px;
	background-color: white;
}

.parkName {
	background-color: #F5F5F5;
	font-size: 15px;
	padding: 10px;
	text-align: center;
}

.parkId {
	padding: 5px;
	margin-top: 0px;
	text-align: right;
	background-color: white;
}

.parkStatus {
	float: right;
	color: #e00909;
}

.cancel {
	border: 1px solid #F5F5F5;
	background-color: white;
}

.orderCount {
	background-color: white;
	margin-top: 10px;
	padding: 5px;
	font-size: 8px;
}

.initPrice {
	color: #C40000;
}

body {
	background-color: #F5F5F5;
}
</style>

</head>
<body>
	<div class="contain">
		<div class="car">
			<span class="am-icon-cab">我的订单</span>
		</div>

		<c:if test="${empty parks}">
			<div class="row carTitless">
				<div class="col-xs-12">没有订单</div>
			</div>
		</c:if>

		<ul id="myTab" class="nav nav-tabs">
			<li class="active"><a href="#dota" data-toggle="tab"> 已预订的订单
			</a></li>
			<li><a href="#lol" data-toggle="tab">已取消的订单</a></li>
		</ul>
		
		<div id="myTabContent" class="tab-content">
   <div class="tab-pane fade in active" id="dota">
		<!-- 添加样式 -->
		<c:if test="${!empty parks}">
			<ul class="list-group">
				<c:forEach items="${parks}" var="parks" varStatus="status">
					<c:if test="${parks.status==1}">
						<div class="orderCount">
							订单 <span class="am-icon-chevron-right"></span>
						</div>
						<div class="arriveTime">
							<div class="">
								<span>停车场起始价</span> <span class="parkStatus"> 待入场 </span>
							</div>
							<div class="initPrice">${parks.parking_lot.init_price}元</div>
						</div>
						<div class="parkName">
							停车场名称： <a
								href="${basepath}/parklot/profile/${parks.user.id}.znck"> <span>${parks.parking_lot.name}</span>
							</a>
						</div>
						<div class="parkId">
							<span>车牌号</span> <span>${parks.car_number}</span>
						</div>
						<div class="cancel am-cf">
							<a href="javascript:doDelete(${parks.id})">
								<button class="am-fr">取消订单</button>
							</a>
						</div>
					</c:if>
				</c:forEach>

			</ul>
		</c:if>
   </div>
   <div class="tab-pane fade" id="lol">
      		<!-- 添加样式 -->
		<c:if test="${!empty parks}">
			<ul class="list-group">
				<c:forEach items="${parks}" var="parks" varStatus="status">
					<c:if test="${parks.status==3}">
						<div class="orderCount">
							订单 <span class="am-icon-chevron-right"></span>
						</div>
						<div class="arriveTime">
							<div class="">
								<span>收费</span> <span class="parkStatus">已取消</span>
							</div>
							<div class="initPrice">${parks.price}元</div>
						</div>
						<div class="parkName">
							停车场：<span>${parks.parking_lot.name}</span>
						</div>
						<div class="parkId">
							<span>预定车牌号</span> <span>${parks.car_number}</span>
						</div>
						<div class="cancel am-cf">
							<a href="${basepath}/parklot/profile/${parks.user.id}.znck">
								<button class="am-fr">再次预定</button>
							</a>
						</div>
					</c:if>
				</c:forEach>

			</ul>
		</c:if>
   </div>
</div>

	</div>
</body>
<script>
	function doDelete(id) {

		$.ajax({
			url : "${basepath}/parklot/order/cancel.znck",
			data : {
				parkId : id
			},
			type : "get",
			async : false,
			success : function(responseText) {
				alert("取消成功");
				window.location.reload();
			},
			error : function() {
				alert("系统错误");
			}
		});
	}
</script>
</html>