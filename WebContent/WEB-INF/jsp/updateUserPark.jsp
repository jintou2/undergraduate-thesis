<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<title>我的停车场</title>

<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<style>
.parkinglotInformation {
	text-align: center;
	border: 1px solid #FFFFFF; 
	margin: 5px;
	font-size: 20px;
	margin : 5px;
}
</style>
<body>
	<div class="container">
		<div class='parkinglotInformation'>
			我的停车位
		</div>
		<c:if test="${!empty userParkinglotsList}">
			<ul class="list-group">
				<c:forEach items="${userParkinglotsList}" var="parkinglotsList">
					<a href="${basepath}/userParklot/profile/${parkinglotsList.id}.znck">
						<li class="list-group-item">
							<div>${parkinglotsList.name}停车场</div>
							<div>
								<span>起始价：${parkinglotsList.init_price}*每小时${parkinglotsList.per_hour_price}/元</span>
							</div>
					</li>
					</a>
				</c:forEach>
			</ul>
		</c:if>
	</div>

	<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
	<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>

	<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
	<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script>
	
	</script>
</body>
</html>