<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="zh-CN">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=eN9cBjl262LzV7Q0dVAE0dcmNY4yzO6H"></script>
<style>
body {
	font-size: 12px;
	font-family: Arial;
}

.imgAndInfo {
	margin: 10px;
}

div.imgInimgAndInfo {
	margin: 10px auto;
	height: 120px;
	border: 1px solid #F5F5F5;;
}

div.parkName {
	margin: 0px 0px 5px;
	background-color: #F5F5F5;
	border: 1px solid #F5F5F5;
	text-align: center;
}

div.carLocation {
	background-color: #F5F5F5;
}

div.parkStatus {
	background-color: #F5F5F5;
	padding: 10px;
}

.carDistance {
	
}

.carName {
	color: black;
	font-size: 20px;
	font-weight: bold;
}

.carInformation {
	background-color: #F5F5F5;
	margin: 5px 0px;
}

.carTitle {
	font-size: 18px;
	font-weight: bold;
	padding: 10px;
	border: 1px solid white;
}

.productPriceDiv {
	width: 50%;
}

.parkEvaluate {
	background-color: #F5F5F5;
}

.sixTr {
	border: 1px solid #F5F5F5;
	height: 100px;
	text-align: center;
	position: relative;
}

.six {
	text-align: center;
	font-size: 25px;
	color: red;
	margin: 5px;
}

.parkInf {
	bolder: 1px solid white;
	text-align: center;
	padding: 20px;
}

.speciction {
	padding: 5px;
	font-size: 8px;
}

.parkSize {
	border: 1px solid white;
	background-color: #F5F5F5;
	width: 100%;
}

.bottom {
	text-align: center;
	background-color: #FFEDED;
	font-size: 16px;
}

.navgation {
	background-color: #FFEDED;
	text-align: center;
	font-size: 16px;
	font-family: arial;
	border: 1px solid white;
}

button {
	background-color: #FFEDED;
	border: 1px solid transparent;
	color: blue;
	font-size: 20px;
	font-weight: bold;
	padding: 5px 0px;
}

.book {
	border: 1px solid white;
}

.carTitle {
	background-color: #F5F5F5;
}

.carId {
	font-size: 18px;
	text-align: center;
}

.carInfor {
	font-size: 18px;
	text-align: center;
}

.inputMoney {
	width: 20%;
	height: 20%;
	border-style: 1px solid white;
}

.startMoney {
	width: 10%;
	height: 20%;
	border-style: 1px solid white;
}

.parkNUmber {
	width: 20%;
	height: 20%;
	border-style: 1px solid white;
}

.car {
	background-color: #dff0d8;
	text-align: center;
	font-size: 20px;
	font-weight: bold;
	color: #3c8584;
	padding: 10px;
}

.carTitless {
	font-size: 18px;
	padding: 10px;
	border: 1px solid white;
	padding-top: 10px;
	background-color: #F5F5F5;
	text-align: center;
}

.carTitle {
	background-color: #dff0d8;
	padding-bottom: 1px;
}

.myOrder {
	font-size: 15px;
	color: #3c8584;
	background-color: #F5F5F5;
	margin-bottom: 20px;
	border: 1px solid white;
}
</style>

</head>
<body>
	<div class="contain">
		<div class="car">
			<span class="am-icon-cab">关于我们</span>
		</div>
		<div class="carTitless">
		     欢迎使用我们的智能停车场，很多功能带完善，见谅！
		</div>
	</div>
</body>

</html>