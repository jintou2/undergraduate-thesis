<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="zh-CN">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css"
	rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=eN9cBjl262LzV7Q0dVAE0dcmNY4yzO6H"></script>
<style>
.error{
	color:red;	
}
body{
width:100%;
}
.contain{
	width:100%;
}
.carInformation{
	background-color: #F5F5F5;
	color: #337AB7;
	text-align:center;
	font-size: 20px;
	font-weight:bold;
	padding:10px;
}
.addOneCar{
	margin:5px;
	border-bottom:1px solid white;
}
.addCar{
	margin: 20px auto;
	font-size: 25px;
	font-weight:bold;
	text-align:center;
	border:1px dotted #96B97D;
	padding: 30px;
}
.atention{
	background-color: #F5F5F5;
	color: #337AB7;
}
.carId{
	font-size: 18px;
	text-align:center;
	padding: 15px;
}
.carTitle{
	border-bottom:1px solid #F5F5F5;
}
.carType{
	font-size: 18px;
	text-align:center;
	padding: 15px;
}
.addCertifiction{
	margin: 10px;
	padding:10px;
	border-bottom:2px solid #F5F5F5;
}
.setButton{
	height: 40px;
}
.selectCarType{
	padding: 15px;
}
.carCert{
	padding: 10px;
	font-size: 18px;
	text-align:center;
}
.atention{
	padding:10px;
}
.submit{
	margin: 0px 5px;
	font-size: 18px;
	font-weight: bold;
}
.mustInputCarId{
	margin:10px;
}
input{
outline:none !important;
}
input:focus {
    outline: none;
    border-color: #cfdc00;
    box-shadow: 0 0 5px rgba(207, 220, 0, 0.4);
}
.Inpt{border:0;outline:none;/*去除蓝色边框*/}
</style>
</head>
<body>
<div class="contain">
<div class="carInformation">填写车辆信息</div>
		<div>
			<div class="carTitle">
				<div class="row">
					<div class="col-xs-4 carId">车牌号码</div>
					<div class="col-xs-7 mustInputCarId">
						<input type="text" class="form-control mustCarId Inpt" placeholder="必修填写车牌号">
					</div>
				</div>
			</div>
			<div>
				<div class="row">
					<div class="col-xs-4 carType">车辆类型</div>
					<div class="col-xs-7">
						<div class="form-group selectCarType">
							<select id="disabledSelect" class="form-control">
								<option>大车</option>
								<option>小车</option>
							</select>
						</div>
					</div>
				</div>
			</div>
<!-- 			<div class="atention">注意信息:请上传真实信息,选择性上传</div>
			<div class="addCertifiction">
				<div class="row">
					<div class="col-xs-4 carCert">上传证件</div>
					<div class="col-xs-6">
						<input type="file">
					</div>
				</div>
			</div> -->
			<div>
				<button type="submit" class="btn btn-block submit">提交</button>
			</div>
		</div>
	</div>
</body>
<script>
$(function(){
	$('.submit').click(function(){
		//$('.mustCarId').blur(function(){
			//var $parent = $(this).parent();
		//	$parent.find('.error').remove();
		//	if($(this).is('.mustCarId')){
			//	var express = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$/;
				//if(this.value == "" || express.test(this.value)==false){
					//$parent.append('<span class="error">'+"请输入正确的车牌"+'</span>');
				//}
			//}
		//});
		
		var number = $('.mustCarId').val();
		var carType = $('#disabledSelect').val();
		var cartypeId= null;
		if(carType=="大车"){
			cartypeId = 1;
		}else{
			cartypeId = 0;
		}
		alert(number+cartypeId);
		$.post('${basepath}/car/cardata/add.znck',{
			cartypeId : cartypeId,
			number : number
		},function(data,textStatus){
				alert(data);
				//window.location.href = "http://www.baidu.com";
		});
	});
});
</script>
</html>