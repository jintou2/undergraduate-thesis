<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE HTML>
<html>
<head>
<title>Graphs</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">	
addEventListener("load", function() { 
	setTimeout(hideURLbar, 0); }, false); 
	function hideURLbar(){ 
		window.scrollTo(0,1); 
		} 
</script>
<!-- Bootstrap Core CSS -->
<link href="${basepath}/inc/template/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="${basepath}/inc/template/css/style.css" rel='stylesheet' type='text/css' />
<link href="${basepath}/inc/template/css/font-awesome.css" rel="stylesheet">
<!-- jQuery -->
<script src="${basepath}/inc/template/js/jquery.min.js"></script>
<!---//webfonts--->
<!-- chart -->
<script src="${basepath}/inc/template/js/Chart.js"></script>
<!-- Nav CSS -->
<link href="${basepath}/inc/template/css/custom.css" rel="stylesheet">
<!-- Metis Menu Plugin JavaScript -->
<script src="${basepath}/inc/template/js/metisMenu.min.js"></script>
<script src="${basepath}/inc/template/js/custom.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${basepath}/inc/template/js/bootstrap.min.js"></script>

<!-- table -->
<!-- Latest compiled and minified CSS -->

<link href="${basepath}/inc/template/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="${basepath}/inc/template/js/bootstrap-editable.min.js"></script>
<style>
.tablepan {
	margin-left: 120px;
	margin-right: 120px;
}

.table {
	margin-bottom: 10px;
}
.editMenuCenter{
	margin-right: 50%;
}
.col-center-block {  
    float: none;  
    display: block;  
    margin-left: auto;  
    margin-right: auto;  
}  
.resetMenu {
	margin-bottom: 5px;
}
.deletePanel{
	float: right;
	margin: 0px auto;
}
.trashSet{
	margin-top: -10px;
	background-color: #3799de;
	 border: none;
    outline: none;
}
.hidden {
	display: none;
}
.show{
	display: block;
}
.topMenu{
	float: left;
	padding-bottom: 0px;
	color: white;
}
.white{
	color: white;
	margin-top: -8px;
}
.setOutLine{
	outline: none;
}
.deleteTr{
	margin-top: -10px;
	background-color: #f9f9f9;
	border: none;
    outline: none;
}
.removeFirstMenu{
	margin-left: 10px;
	margin-bottom: 5px;
}
</style>
</head>

	<%@ include file="common/header.jsp"%>
	<div id="page-wrapper">
		<div class="graphs">
			<div class="grid_3 grid_5">
				<div class="row">
				<div class="col-xs-6 ">
					<button class="btn btn_5 btn-lg btn-info mx-auto col-center-block" id="editMenu">
						<span class="glyphicon glyphicon-edit"></span> 添加菜单
					</button>
				</div>
				<div class="col-xs-6">
					<button  class="btn btn_5 btn-lg btn-warning warning_11 edit editMenuCenter col-center-block" id="getMenuValue">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> 保存当前配置
					</button>
				</div>
			</div>
		</div>
		
		<c:forEach items = "${SynthesisMenus}" var = "SynthesisMenu" varStatus = "status">
					<div class="panel panel-warning">
						<div class="panel-heading">
						    <div class="topMenu">菜单</div>
							<span class = "deletePanel">
								<button class ="trashSet">
									<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i> 
								</button>   	
   							</span>
						</div>
	
						<div class="panel-body no-padding menus" style="display: block;">
						<!-- ********************************有没有一级菜单******************************* -->
						
							<div class='input-group form-inline pull-left firstMenu'>
								一级菜单：
									<c:if test='${SynthesisMenu.getSub_button().isEmpty()}'>		
										<a href="#" class="btType">无一级菜单</a>
									</c:if>
									<c:if test='${SynthesisMenu.getSub_button().size() > 0}'>		
										<a href="#" class="btType">${SynthesisMenu.name}</a>
									</c:if>
									<button class='btn btn-sm removeFirstMenu'>
										<span class='glyphicon glyphicon-minus'></span>
									</button>
									
							</div>

								<button class='btn btn-sm pull-right addTd'>
										<span class='glyphicon glyphicon-plus'></span>
								</button>
								<table class="table table-striped">
									<thead>
										<tr class="warning">
											<th>type值</th>
											<th>name值</th>
											<th>类型</th>
											<th>url/key值</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test='${SynthesisMenu.getSub_button().size() > 0}'>
											<c:forEach items = "${SynthesisMenu.sub_button}" var = "SynthesisMenuSubBt" varStatus = "status">	
												<tr>
													<td>
														<a href="#" class="clickOrView">${SynthesisMenuSubBt.type}</a>
													</td>
													<td>
														<a href="#" class="btType menuName">${SynthesisMenuSubBt.name}</a>
													</td>
													<td>
														<c:if test="${SynthesisMenuSubBt.url !=null}">
															<a href="#" class="urlOrType " >url</a>
														</c:if>
														<c:if test="${SynthesisMenuSubBt.url ==null}">
															<a href="#" class="urlOrType " >key</a>
														</c:if>
													</td>
													<td>
														<c:if test="${SynthesisMenuSubBt.url !=null}" >
															<a href="#" class="btType urlOrKeyValue" >${SynthesisMenuSubBt.url}</a>
														</c:if>
														<c:if test="${SynthesisMenuSubBt.url ==null}">
															<a href="#" class="btType urlOrKeyValue" >${SynthesisMenuSubBt.key}</a>
														</c:if>
													</td>
													<td>
														<button class ="deleteTr">
															<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i> 
														</button>   
													</td>
												</tr>
											</c:forEach>
										</c:if>
										<c:if test='${SynthesisMenu.getSub_button().isEmpty()}'>
												<tr>
													<td>
														<a href="#" class="clickOrView">${SynthesisMenu.type}</a>
													</td>
													<td>
														<a href="#" class="btType menuName">${SynthesisMenu.name}</a>
													</td>
													<td>
														<c:if test="${SynthesisMenu.url !=null}">
															<a href="#" class="urlOrType " >url</a>
														</c:if>
														<c:if test="${SynthesisMenu.key !=null}">
															<a href="#" class="urlOrType " >key</a>
														</c:if>
													</td>
													<td>
														<c:if test="${SynthesisMenu.url !=null}" >
															<a href="#" class="btType urlOrKeyValue" >${SynthesisMenu.url}</a>
														</c:if>
														<c:if test="${SynthesisMenu.key !=null}">
															<a href="#" class="btType urlOrKeyValue" >${SynthesisMenu.key}</a>
														</c:if>
													</td>
													<td>
														<button class ="deleteTr">
															<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i> 
														</button>   
													</td>
												</tr>
										</c:if>
									</tbody>
								</table>
							</div>
							<!-- *************不是复合菜单************** -->
						</div>
					</c:forEach>
					
		</div>
	</div>

	<!-- /#wrapper -->
	
<script type="text/javascript">
	$(function(){
	    var addTrValue = '<tr>'+
	    				  '<td><a href="#" class="clickOrView">view</a></td>'+
	    				  '<td><a href="#" class="btType menuName">name值</a></td>'+
	    				  '<td><a href="#" class="urlOrType" >url</a></td>'+
	    				  '<td><a href="#" class="btType urlOrKeyValue" >http://znck.frpgz1.idcfengye.com/WeiXinZNCK</a></td>'+
	    				  '<td><button class ="deleteTr"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button> </td>'+
	    				  '</tr>';
	    var menus = '<a href="#" class="btType">无一级菜单</a>'+
			'<button class="btn btn-sm removeFirstMenu"><span class="glyphicon glyphicon-minus"></span>'+
			'</button>';
			
		btType();
		urlOrType();
		clickOrView();
		
		//********************添加整个安虐********************//
		$('#editMenu').click(function(){
			 var panelLength = $('.panel').length;
			 if(panelLength > 2){
				 alert('不能添加菜单');
				 return false;
			 }
			 var $panel = $('.panel').eq(0).clone(true);
			 $panel.find("tbody tr").remove();
			 $panel.find(".firstMenu").children().remove();
			 var $graphs = $('.graphs');
			 var $firstMenu = $panel.find('.firstMenu');
			 $firstMenu.append(menus);
			 $graphs.append($panel);
			btType();
			urlOrType();
			clickOrView();
			alert("添加成功");
			//***********删除一级菜单***************//
			$('.removeFirstMenu').click(function(){
				var trSize = $(this).parent().parent().find('table').find('tbody').find('tr').length;
				if(trSize > 1){
					alert("不能删除一级菜单");
					return false;
				}else{
					$(this).parent().remove();
				}
			});
		});
		
		//**************删除整个菜单***********//
		$('.deletePanel').click(function(){
			if(!confirm("确定删除？")){
				return false;
			}else{
				$(this).parent().parent().remove();
			}
		});
		//****************编辑table文本款**********//
		function btType(){
		$("body .btType").editable({
			type: "text",
			 disabled: false,             //是否禁用编辑
			 mode: "popup",              //编辑框的模式：支持popup和inline两种模式，默认是popup
	         validate: function (value) { //字段验证
	                if (!$.trim(value)) {
	                    return '不能为空';
	                }
	            }
		});
		}
		//*******************编辑可选框****************//
		function urlOrType(){
		 $('body .urlOrType').editable({
	            type: "select",              //编辑框的类型。支持text|textarea|select|date|checklist等
	            source: [{ value: 1, text: "key" }, { value: 2, text: "url" }],
	            title: "选择类型",           //编辑框的标题
	            disabled: false,           //是否禁用编辑
	            emptytext: "空文本",       //空值的默认文本
	            mode: "popup",            //编辑框的模式：支持popup和inline两种模式，默认是popup
	            validate: function (value) { //字段验证
	                if (!$.trim(value)) {
	                    return '不能为空';
	                }
	            }
	        });
		}
		
		//*************************click类型或者view类型**************************//
		//编辑可选框
		function clickOrView(){
		 $('.clickOrView').editable({
	            type: "select",              //编辑框的类型。支持text|textarea|select|date|checklist等
	            source: [{ value: 1, text: "view" }, { value: 2, text: "click" }],
	            title: "选择类型",           //编辑框的标题
	            disabled: false,           //是否禁用编辑
	            emptytext: "空文本",       //空值的默认文本
	            mode: "popup",            //编辑框的模式：支持popup和inline两种模式，默认是popup
	            validate: function (value) { //字段验证
	                if (!$.trim(value)) {
	                    return '不能为空';
	                }
	            }
	        });
		}
		$("body").on("click", ".deleteTr", function(e) {
			// remove text box 
			if(!confirm("确定删除!")){
				return false;
			}
				$(this).parent().parent().remove();	
		});
		
		//********************添加tr****************//
		$('.addTd').click(function(){
			var tables = $(this).next().children("tbody");
			//var tr = $(this).next().children("tbody tr:last-child").append(input);
	    	tables.append(addTrValue);
			btType();
			urlOrType();
			clickOrView();
		});
		
		//**********************删除安虐***********************//
		$("body").on("click", ".removeclass", function(e) {
			// remove text box  
			$(this).remove();
		});
		
		
		
		//**********************获取数值***********************//
		$('#getMenuValue').click(function(){
			var rootMenu = {};//外层menu菜单
			var menuButton = new Array();
			var object = new Object(); //json对象
			var AllButton = new Array();
			//***********遍历menus**********//
			$("div").find(".menus").each(function(){
				var firstMenu = $(this).find(".firstMenu");
				var tbodyEl = $(this).find("table").children("tbody");
				//是复合菜单的情况
				if(firstMenu.length>0){
					var oneMenu = { };//外边这个大括号
					var subBt = new Array();//button数组
					var firstMenuValue = firstMenu.children("a").text();//一级菜单的值
					if(firstMenuValue !="无一级菜单"){
						oneMenu["name"] = firstMenuValue;
					}
					//********遍历tr*******//
					tbodyEl.find("tr").each(function(){
						var tdArr = $(this).children();
						var secondMenuObj = {};//一个菜单项
						var clickOrView = tdArr.eq(0).find('.clickOrView').text();
						var name =  tdArr.eq(1).find('.menuName').text();
						var urlOrType = tdArr.eq(2).find('.urlOrType').text();
						var urlOrKeyValue = tdArr.eq(3).find('.urlOrKeyValue').text();
						//console.log(clickOrView + urlOrType + urlOrKeyValue);
						
						secondMenuObj["type"] = clickOrView;
						secondMenuObj["type"] = clickOrView;
						secondMenuObj["name"] = name;
						secondMenuObj[urlOrType] = urlOrKeyValue;
						//secondMenuObj["sub_button"] = "[ ]";
						var secondMenuObjToString = JSON.stringify(secondMenuObj);
						if(firstMenuValue !="无一级菜单"){
							subBt.push(secondMenuObj);
						}if(firstMenuValue =="无一级菜单"){
							AllButton.push(secondMenuObj);//构建button数组
						}
					});
					//console.log(subBt);
					if(firstMenuValue !="无一级菜单"){
						oneMenu["sub_button"] = subBt;//添加子菜单里边的sub_button'
						AllButton.push(oneMenu);//构建button数组
					}
					//console.log(subBt);
					
					//console.log(AllButton);
					
					//console.log(menuButton);
				}//***********if结束********
				//***********判断没有一级菜单的情况********
				else{
					tbodyEl.find("tr").each(function(){
						var tdArr = $(this).children();
						var secondMenuObj = {};//一个菜单项
						var clickOrView = tdArr.eq(0).find('.clickOrView').text();
						var name =  tdArr.eq(1).find('.menuName').text();
						var urlOrType = tdArr.eq(2).find('.urlOrType').text();
						var urlOrKeyValue = tdArr.eq(3).find('.urlOrKeyValue').text();
						//console.log(clickOrView + urlOrType + urlOrKeyValue);
						secondMenuObj["type"] = clickOrView;
						secondMenuObj["name"] = name;
						secondMenuObj[urlOrType] = urlOrKeyValue;
						//secondMenuObj["sub_button"] = "[ ]";只有一个一级菜单无此选项
						var secondMenuObjToString = JSON.stringify(secondMenuObj);
						AllButton.push(secondMenuObj);//构建button数组
					});
				}//***********else结束********
			});
			rootMenu["button"] = AllButton;
			var menuValue = JSON.stringify(rootMenu);
			alert(menuValue);
			$.post("${basepath}/weixin/menu/modify.znck",
						{ menujson : menuValue},
							function(data,testStatus){
							var obj = JSON.parse(data);
							if(obj.result==true){
								alert("修改成功！");
								
							}else{
								alert("修改失败，请添加合法的菜单！");
							}
					});
			
		});
		
		//***********删除一级菜单***************//
		$('.removeFirstMenu').click(function(){
			var trSize = $(this).parent().parent().find('table').find('tbody').find('tr').length;
			if(trSize > 1){
				alert("不能删除一级菜单");
				return false;
			}else{
				$(this).parent().remove();
			}
		});
		
});
</script>
