<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="zh-CN">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="${basepath}/inc/login/css/amazeui.min.css">
<script src="${basepath}/inc/js/jquery/2.0.0/jquery.min.js"></script>
<link href="${basepath}/inc/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<script src="${basepath}/inc/js/bootstrap/3.3.6/bootstrap.min.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=eN9cBjl262LzV7Q0dVAE0dcmNY4yzO6H"></script>
<style>
body {
	font-size: 12px;
	font-family: Arial;
}

.imgAndInfo {
	margin: 10px;
}

div.imgInimgAndInfo {
	margin: 10px auto;
	height: 120px;
	border: 1px solid #F5F5F5;;
}

div.parkName {
	margin: 0px 0px 5px;
	background-color: #F5F5F5;
	border: 1px solid #F5F5F5;
	text-align: center;
}

div.carLocation {
	background-color: #F5F5F5;
}

div.parkStatus {
	background-color: #F5F5F5;
	padding: 10px;
}

.carDistance {
	
}

.carName {
	color: black;
	font-size: 20px;
	font-weight: bold;
}

.carInformation {
	background-color: #F5F5F5;
	margin: 5px 0px;
}

.carTitle {
	font-size: 18px;
	font-weight: bold;
	padding: 10px;
	border: 1px solid white;
}

.productPriceDiv {
	width: 50%;
}

.parkEvaluate {
	background-color: #F5F5F5;
}

.sixTr {
	border: 1px solid #F5F5F5;
	height: 100px;
	text-align: center;
	position: relative;
}

.six {
	text-align: center;
	font-size: 25px;
	color: red;
	margin: 5px;
}

.parkInf {
	bolder: 1px solid white;
	text-align: center;
	padding: 20px;
}

.speciction {
	padding: 5px;
	font-size: 8px;
}

.parkSize {
	border: 1px solid white;
	background-color: #F5F5F5;
	width: 100%;
}

.bottom {
	text-align: center;
	background-color: #FFEDED;
	font-size: 16px;
}

.navgation {
	background-color: #FFEDED;
	text-align: center;
	font-size: 16px;
	font-family: arial;
	border: 1px solid white;
}

button {
	background-color: #FFEDED;
	border: 1px solid transparent;
	color: blue;
	font-size: 20px;
	font-weight: bold;
	padding: 5px 0px;
}

.book {
	border: 1px solid white;
}

.carTitle {
	background-color: #F5F5F5;
}

.carId {
	font-size: 18px;
	text-align: center;
}

.carInfor {
	font-size: 18px;
	text-align: center;
}

.inputMoney {
	width: 20%;
	height: 20%;
	border-style: 1px solid white;
}

.startMoney {
	width: 10%;
	height: 20%;
	border-style: 1px solid white;
}

.parkNUmber {
	width: 20%;
	height: 20%;
	border-style: 1px solid white;
}

.car {
	background-color: #dff0d8;
	text-align: center;
	font-size: 20px;
	font-weight: bold;
	color: #3c8584;
	padding: 10px;
}

.carTitless {
	font-size: 18px;
	padding: 10px;
	border: 1px solid white;
	padding-top: 10px;
	background-color: #F5F5F5;
	text-align: center;
}

.carTitle {
	background-color: #dff0d8;
	padding-bottom: 1px;
}

.myOrder {
	font-size: 15px;
	color: #3c8584;
	background-color: #F5F5F5;
	margin-bottom: 20px;
	border: 1px solid white;
}
</style>

</head>
<body>
	<div class="contain">
		<div class="car"> 
			<span class="am-icon-cab">停车场详细信息</span>
			<button class="btn btn-primary btn-sm myOrder pull-right myOrders">
				<span class="glyphicon glyphicon-user "></span> 我的订单
			</button>
		</div>
		<div class="carTitless">
			<div class="row">
				<div class="col-xs-6">停车场名称</div>
				<div class="col-xs-6">
					<div class=" parkName">${profile.name}</div>
				</div>
			</div>
		</div>
		<div class="parkStatus">
			<div class="row">
				<div class="col-xs-6 carId">停车场位置</div>
				<div class="col-xs-6 parkName">
					${profile.address} 
					<span class="am-icon-btn am-icon-arrow-up goHere"></span>
				</div>
			</div>

		</div>
		<div class="carInformation">
			<div class="carTitle">
				<p>
					<span class="am-icon-cab"></span> 
					车位信息
				</p>
			</div>
			<div class="row parkInf">
				<div class="col-xs-6">
					<span class="sixTr"> <span class="six">${profile.per_hour_price}</span>元/小时
					</span>
					<div class="speciction">起始价${profile.init_price}元</div>
				</div>
				<div class="col-xs-6">
					<span class="sixTr"> <span class="six">${profile.surplus_park_num}</span>个空车位
					</span>
				</div>
			</div>

			<div>
				<div class="row">
					<div class="col-xs-6 carId">车辆类型</div>
					<div class="col-xs-6 parkName">
					小车
						<div class="form-group selectCarType">
						<c:forEach items="${profile.cartypes}" var="cartypes">
							<c:out value="${cartypes.type}"></c:out>
						</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="productSaleAndReviewNumber">
			<div class="carTitle">
				<p>
					<span class="am-icon-edit"></span> 描述
				</p>
			</div>

			<div class="panel panel-default">
				<div class="panel-body">${profile.description}</div>
			</div>
		</div>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button data-dismiss="modal" class="close" type="button">
							<span aria-hidden="true">×</span><span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title">输入车牌号</h4>
					</div>
					<div class="modal-body">
						<p>请输入当前车牌号</p>
						<textarea class="form-control"></textarea>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default" type="button">关闭</button>
						<button class="btn btn-primary orderPark" type="button">提交</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>

		<div style="height: 200px"></div>
		<nav class="navbar navbar-default navbar-fixed-bottom bottom">
			<div class="bottom">
				<input type="button" class="btn btn-success btn-block submit" id="open" value = "预定">
			</div>
		</nav>
		<div style="clear: both"></div>
	</div>
</body>
<script>
	$(function() {
		
		//查看我的订单
		$(".myOrders").click(function(){
			 window.location.href = "${basepath}/user/myorder.znck";
		});
		//获取指定cookie
		function getCookie(c_name) {
			if (document.cookie.length > 0) {
				c_start = document.cookie.indexOf(c_name + "=");
				if (c_start != -1) {
					c_start = c_start + c_name.length + 1;
					c_end = document.cookie.indexOf(";", c_start);
					if (c_end == -1)
						c_end = document.cookie.length;
					return unescape(document.cookie.substring(c_start, c_end));
				}
			}
			return "";
		}

		var geoc = new BMap.Geocoder();
		var parkLat = ${profile.longitude};
		var parkLng = ${profile.latitude};
		var parkId = ${profile.id};
		var lat = getCookie("lat");
		var lng = getCookie("lng");
		var times = getCookie("time");
		var myLocation = new BMap.Point(lng, lat);
		var destination =  "${profile.address}";
		geoc.getLocation(myLocation,function(result) {
							//逆地址转换
							var addCom = result.addressComponents;
							var city = addCom.city;
							var address = destination;
							$('.goHere').click(function() {
											    window.location.href="http://api.map.baidu.com/direction?origin=latlng:"+myLocation.lat +","+myLocation.lng+"|name:我的位置&destination=" 
									    		+ address
									    		+ "&mode=driving&region="+ city +"&output=html&src=yourCompanyName|yourAppName";
											});
						});

		// 预定,界面button根据status设置值
		var status = ${profile.status};
		$('.submit').click(function() {
			                var order = $('.submit').val();
			                if(order == "预定"){
							Date.prototype.pattern = function(fmt) {
								var o = {
									"M+" : this.getMonth() + 1, //月份         
									"d+" : this.getDate(), //日         
									"h+" : this.getHours() % 12 == 0 ? 12: this.getHours() % 12, //小时         
									"H+" : this.getHours(), //小时         
									"m+" : this.getMinutes(), //分         
									"s+" : this.getSeconds(), //秒         
									"q+" : Math.floor((this.getMonth() + 3) / 3),
									"S" : this.getMilliseconds()
								//毫秒         
								};
								var week = {
									"0" : "/u65e5",
									"1" : "/u4e00",
									"2" : "/u4e8c",
									"3" : "/u4e09",
									"4" : "/u56db",
									"5" : "/u4e94",
									"6" : "/u516d"
								};
								if (/(y+)/.test(fmt)) {
									fmt = fmt.replace(RegExp.$1, (this
											.getFullYear() + "")
											.substr(4 - RegExp.$1.length));
								}
								if (/(E+)/.test(fmt)) {
									fmt = fmt
											.replace(
													RegExp.$1,
													((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "/u661f/u671f"
															: "/u5468")
															: "")
															+ week[this
																	.getDay()
																	+ ""]);
								}
								for ( var k in o) {
									if (new RegExp("(" + k + ")").test(fmt)) {
										fmt = fmt.replace(
													RegExp.$1,(RegExp.$1.length == 1) ? (o[k]): (("00" + o[k]).substr(("" + o[k]).length)));
									}
								}
								return fmt;
							}
							var date = new Date();
							var x = date.pattern("yyyy-MM-dd hh:mm:ss"); // 取得的TextBox中的时间         
							var time = new Date(x.replace("-", "/"));
							var b = parseInt(times);
							time.setMinutes(time.getMinutes() + b, time.getSeconds(), 0);
							var expectTime = time.pattern("yyyy-MM-dd hh:mm:ss");
							
							$("#myModal").modal('show');
							//输入车牌号
							$('.orderPark').click(function(){
								var orderPark = $('.form-control').val();
								$.post('${basepath}/parklot/order.znck',{
									parklotId: parkId,
									car_number: orderPark,
									EstimateArrival: expectTime
								},function(data, textStatus){	
								var obj = JSON.parse(data);
								if(obj.status == 1){
										alert('预定成功');
									}
								if(obj.status == 5){
									alert('输入的车牌已有订单，预定失败');
								}
								window.location.reload();
								});
							});
			                }else{
			                	alert("很好的");
			                }
						});
	});
</script>
</html>